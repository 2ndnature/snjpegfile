//
//  SNJPEGFile+ConvenienceMethods.m
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain.
//

#import "SNJPEGFile+ConvenienceMethods.h"
#if TARGET_OS_IPHONE
#import <MobileCoreServices/MobileCoreServices.h>
#endif

@implementation SNJPEGFile (ConvenienceMethods)

- (void)setStarRating:(NSUInteger)rating
{
    id value = (rating == 0) ? [NSNull null] : @(rating);
    
    [self mergeMetadata:@{ (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCStarRating:value } }];
}

- (void)setGPSLocation:(nullable CLLocation *)location
{
    if ([location isKindOfClass:[CLLocation class]] == NO)
    {
        [self mergeMetadata:@{ (id)kCGImagePropertyGPSDictionary:[NSNull null] }];
    }
    else
    {
        NSMutableDictionary *value = [NSMutableDictionary dictionary];
        
        [value setObject:@[@(2),@(2),@(0),@(0)] forKey:(id)kCGImagePropertyGPSVersion];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm:ss.SSSSSS"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [value setObject:[formatter stringFromDate:location.timestamp] forKey:(id)kCGImagePropertyGPSTimeStamp];
        [formatter setDateFormat:@"yyyy:MM:dd"];
        [value setObject:[formatter stringFromDate:location.timestamp] forKey:(id)kCGImagePropertyGPSDateStamp];
        
        double latitude = location.coordinate.latitude;
        if (latitude < 0)
        {
            latitude = -latitude;
            [value setObject:@"S" forKey:(id)kCGImagePropertyGPSLatitudeRef];
        }
        else
        {
            [value setObject:@"N" forKey:(id)kCGImagePropertyGPSLatitudeRef];
        }
        [value setObject:@(latitude) forKey:(id)kCGImagePropertyGPSLatitude];
        
        double longitude = location.coordinate.longitude;
        if (longitude < 0)
        {
            longitude = -longitude;
            [value setObject:@"W" forKey:(id)kCGImagePropertyGPSLongitudeRef];
        }
        else
        {
            [value setObject:@"E" forKey:(id)kCGImagePropertyGPSLongitudeRef];
        }
        [value setObject:@(longitude) forKey:(id)kCGImagePropertyGPSLongitude];
        
        double altitude = location.altitude;
        if (!isnan(altitude))
        {
            if (altitude < 0)
            {
                altitude = -altitude;
                [value setObject:@1 forKey:(id)kCGImagePropertyGPSAltitudeRef];
            }
            else
            {
                [value setObject:@0 forKey:(id)kCGImagePropertyGPSAltitudeRef];
            }
            [value setObject:@(altitude) forKey:(id)kCGImagePropertyGPSAltitude];
        }
        
        if (location.speed >= 0)
        {
            [value setObject:@"K" forKey:(id)kCGImagePropertyGPSSpeedRef];
            [value setObject:@(location.speed * 3.6) forKey:(id)kCGImagePropertyGPSSpeed];
        }
        
        if (location.course >= 0)
        {
            [value setObject:@"T" forKey:(id)kCGImagePropertyGPSTrackRef];
            [value setObject:@(location.course) forKey:(id)kCGImagePropertyGPSTrack];
        }

        [self mergeMetadata:@{ (id)kCGImagePropertyGPSDictionary:value }];
    }
}

- (void)setCopyright:(nullable NSString *)copyright
{
    id value = ([copyright isKindOfClass:[NSString class]] == NO) ? [NSNull null] : copyright;
    
    [self mergeMetadata:@{
                          (id)kCGImagePropertyTIFFDictionary:@{ (id)kCGImagePropertyTIFFCopyright:value },
                          (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCCopyrightNotice:value }
                          }];
}

- (void)setCaption:(nullable NSString *)caption
{
    id value = ([caption isKindOfClass:[NSString class]] == NO) ? [NSNull null] : caption;
    
    [self mergeMetadata:@{ (id)kCGImagePropertyTIFFDictionary:@{ (id)kCGImagePropertyTIFFImageDescription:value } }];
}

- (void)setByline:(nullable NSString *)byline
{
    id value = ([byline isKindOfClass:[NSString class]] == NO) ? [NSNull null] : byline;
    
    [self mergeMetadata:@{ (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCByline:value } }];
}

- (void)setHeadline:(nullable NSString *)headline
{
    id value = ([headline isKindOfClass:[NSString class]] == NO) ? [NSNull null] : headline;
    
    [self mergeMetadata:@{ (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCHeadline:value } }];
}

- (void)setUsageTerms:(nullable NSString *)usageTerms
{
    id value = ([usageTerms isKindOfClass:[NSString class]] == NO) ? [NSNull null] : usageTerms;
    
    [self mergeMetadata:@{ (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCRightsUsageTerms:value } }];
}

- (void)setKeywords:(nullable NSArray<NSString *> *)keywords
{
    id value = ([keywords isKindOfClass:[NSArray class]] == NO) ? [NSNull null] : keywords;
    
    [self mergeMetadata:@{ (id)kCGImagePropertyIPTCDictionary:@{ (id)kCGImagePropertyIPTCKeywords:value } }];
}

#pragma mark - Methods

- (void)wipeMetadata
{
    [self setMetadata:nil];
}

- (void)wipeGPS
{
    [self setGPSLocation:nil];
}

- (void)rotateClockwise
{
    [self rotateClockwise:YES];
}

- (void)rotateCounterclockwise
{
    [self rotateClockwise:NO];
}

- (void)rotateClockwise:(BOOL)clockwise
{
    NSNumber *newOrientation;
    switch ([[self.metadata objectForKey:(id)kCGImagePropertyOrientation] integerValue])
    {
        default:
        case 1:
            newOrientation = (clockwise) ? @6 : @8;
            break;
        case 2:
            newOrientation = (clockwise) ? @7 : @5;
            break;
        case 3:
            newOrientation = (clockwise) ? @8 : @6;
            break;
        case 4:
            newOrientation = (clockwise) ? @5 : @7;
            break;
        case 5:
            newOrientation = (clockwise) ? @2 : @4;
            break;
        case 6:
            newOrientation = (clockwise) ? @3 : @1;
            break;
        case 7:
            newOrientation = (clockwise) ? @4 : @2;
            break;
        case 8:
            newOrientation = (clockwise) ? @1 : @3;
            break;
    }
    [self mergeMetadata:@{ (id)kCGImagePropertyOrientation:newOrientation }];
}

+ (nullable NSURL *)fileByResizingFile:(nonnull NSURL *)fileURL toMaximumDimension:(NSUInteger)maxDimension
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileURL.path] == NO) return nil;

    NSURL *result = nil;
    CGImageSourceRef imgSrc = nil;
    NSString *type = CFBridgingRelease(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[fileURL pathExtension], NULL));
    if (type == nil) type = @"public.jpeg";

    CGDataProviderRef provider = CGDataProviderCreateWithURL((CFURLRef)fileURL);
    if (provider != nil)
    {
        imgSrc = CGImageSourceCreateWithDataProvider(provider,(CFDictionaryRef)@{
                                                                                 (id)kCGImageSourceShouldAllowFloat:@YES,
                                                                                 (id)kCGImageSourceTypeIdentifierHint:type
                                                                                 });
        CGDataProviderRelease(provider);
    }
    if (imgSrc != nil)
    {
        if (CGImageSourceGetCount(imgSrc) > 0)
        {
            BOOL isJPEG = NO;
            NSFileHandle *fh = [NSFileHandle fileHandleForReadingAtPath:fileURL.path];
            [fh seekToEndOfFile];
            if (fh.offsetInFile > 2)
            {
                [fh seekToFileOffset:0];
                UInt16 soi = 0xD8FF;
                isJPEG = [[fh readDataOfLength:2] isEqualToData:[NSData dataWithBytes:&soi length:2]];
            }
            [fh closeFile];
            NSDictionary *metadata = nil;
            if (isJPEG == NO)
            {
                metadata = CFBridgingRelease(CGImageSourceCopyPropertiesAtIndex(imgSrc, 0, (CFDictionaryRef)@{(id)kCGImageSourceShouldAllowFloat:@YES, (id)kCGImageSourceTypeIdentifierHint:CFBridgingRelease(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileURL.path.pathExtension, CFSTR("public.data")))}));
            }
            CGImageRef imageRef;
            if (maxDimension > 0)
            {
                imageRef = CGImageSourceCreateThumbnailAtIndex(imgSrc, 0, (CFDictionaryRef)@{
                                                                                             (id)kCGImageSourceCreateThumbnailWithTransform:@YES,
                                                                                             (id)kCGImageSourceThumbnailMaxPixelSize:@(maxDimension),
                                                                                             (id)kCGImageSourceCreateThumbnailFromImageAlways:@YES
                                                                                             });
            }
            else
            {
                imageRef = CGImageSourceCreateThumbnailAtIndex(imgSrc, 0, (CFDictionaryRef)@{
                                                                                             (id)kCGImageSourceCreateThumbnailWithTransform:@YES,
                                                                                             (id)kCGImageSourceCreateThumbnailFromImageAlways:@YES
                                                                                             });
            }
            if (imageRef)
            {
                NSURL *outputFile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingPathExtension:@"JPG"]]];
                CGImageDestinationRef imgDst = CGImageDestinationCreateWithURL((CFURLRef)outputFile, (CFStringRef)@"public.jpeg", 1, NULL);
                if (imgDst)
                {
                    CGImageDestinationAddImage(imgDst, imageRef, (CFDictionaryRef)metadata);
                    if (CGImageDestinationFinalize(imgDst))
                    {
                        if (isJPEG)
                        {
                            [SNJPEGFile copyMetadataFromFile:fileURL toFile:outputFile];
                        }
                        result = outputFile;
                    }
                    CFRelease(imgDst);
                }
                CGImageRelease(imageRef);
            }
        }
        CFRelease(imgSrc);
    }
    return result;
}

@end
