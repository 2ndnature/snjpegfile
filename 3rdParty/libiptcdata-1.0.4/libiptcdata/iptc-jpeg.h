/* iptc-jpeg.h
 *
 * Copyright � 2005 David Moore <dcm@acm.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __IPTC_JPEG_H__
#define __IPTC_JPEG_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdio.h>
#include <libiptcdata/iptc-data.h>

int iptc_jpeg_read_ps3 (FILE * infile, unsigned char * buf, unsigned int size);
int iptc_jpeg_ps3_find_iptc (const unsigned char * ps3,
		unsigned int ps3_size, unsigned int * iptc_len);

int iptc_jpeg_ps3_save_iptc (const unsigned char * ps3, unsigned int ps3_size,
		const unsigned char * iptc, unsigned int iptc_size,
		unsigned char * buf, unsigned int size);
int iptc_jpeg_save_with_ps3 (FILE * infile, FILE * outfile,
		const unsigned char * ps3, unsigned int ps3_size);

/*! Copies an APP13 header with an 8BIM entry containing the IPTC data to the
 * supplied buffer.
 *
 * \param[in] data The IPTC data object.
 * \param[in] extra Optional extra 8BIM data to append after the IPTC block.
 * \param[in] extras Size of the extra data.
 * \param[out] buf Pointer to buffer pointer containing the APP13 header, raw
 *   IPTC data, and optional extra data on return.
 * \param[out] size Pointer to variable to hold the number of bytes of
 *   data at buf, or set to 0 on error.
*/
void iptc_data_save_data (IptcData *data, const unsigned char *extra,
        unsigned int extras, unsigned char **buf, unsigned int *size);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __IPTC_JPEG_H__ */
