/* jpeg-data.c
 *
 * Copyright � 2001 Lutz M�ller <lutz@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA.
 */

#include "jpeg-data.h"

#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "iptc-jpeg.h"

static const unsigned char ExifHeader[] = "Exif";
static const unsigned char XMPHeader[] = "http://ns.adobe.com/xap/1.0/";

/* exif-i18n.h used to be imported here.
 */
#define _(String) (String)

/* realloc that cleans up on memory failure and returns to caller */
#define CLEANUP_REALLOC(p,s) { \
    if (data->fileDescriptor == -1) { \
        unsigned char *cleanup_ptr = realloc((p),(s)); \
        if (!cleanup_ptr) { free(p); (p) = NULL; return; } \
        (p) = cleanup_ptr; \
    } \
}

struct _JPEGDataPrivate
{
	unsigned int ref_count;

	ExifLog *log;
};

JPEGData *
jpeg_data_new (void)
{
	JPEGData *data;

	data = malloc (sizeof (JPEGData));
	if (!data)
		return (NULL);
	memset (data, 0, sizeof (JPEGData));
	data->priv = malloc (sizeof (JPEGDataPrivate));
	if (!data->priv) {
		free (data);
		return (NULL);
	}
	memset (data->priv, 0, sizeof (JPEGDataPrivate));
	data->priv->ref_count = 1;
    
    data->fileData = NULL;
    data->fileDescriptor = -1;
    data->fileSize = 0;
    data->imageOffset = 0;

	return (data);
}

void
jpeg_data_append_section (JPEGData *data)
{
    JPEGSection *s;
    
    if (!data) return;
    
    if (!data->count)
        s = malloc (sizeof (JPEGSection));
    else
        s = realloc (data->sections,
                     sizeof (JPEGSection) * (data->count + 1));
    if (!s) {
        EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data",
                            sizeof (JPEGSection) * (data->count + 1));
        return;
    }
    memset(s + data->count, 0, sizeof (JPEGSection));
    data->sections = s;
    data->count++;
}

void
jpeg_data_remove_section (JPEGData *data, JPEGSection *section)
{
    if (!data) return;
    if (!section) return;
    
    JPEGSection *s;
    unsigned int i;

    if (section->content.app13.additional_data) {
        free (section->content.app13.additional_data);
        section->content.app13.additional_data = NULL;
    }
    if (section->content.generic.data) {
        free (section->content.generic.data);
        section->content.generic.data = NULL;
    }
 
    for (i = 0; i < data->count; i++) {
        if (&data->sections[i] == section) {
            if (i + 1 < data->count) {
                memmove (&data->sections[i], &data->sections[i + 1],
                         sizeof (JPEGSection) * (data->count - (i + 1)));
            }
            break;
        }
    }
    if (data->count == 1) {
        free (data->sections);
        s = NULL;
    }
    else {
        s = realloc (data->sections,
                     sizeof (JPEGSection) * (data->count - 1));
    }
    data->sections = s;
    data->count--;
}

/*! jpeg_data_save_file returns 1 on success, 0 on failure */
int
jpeg_data_save_file (JPEGData *data, const char *path)
{
    int fd = -1;
    unsigned char *d = NULL;
    unsigned int size = 0, temp_size;

    
    if ((fd = open(path, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0666)) == -1) {
        perror("Error opening file");
        return 0;
    }

    /* FIXME: Assuming headers will never be more than a megabyte.
       Find a better way of ball parking it. Loop through headers and add
       up the size, and finish off adding 1024 of padding?
     */
    temp_size = data->fileSize + (1024 * 1024);

    if (lseek(fd, temp_size - 1, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        return 0;
    }
    
    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        return 0;
    }
    
    d = (unsigned char *)mmap(NULL, temp_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    if (d == MAP_FAILED) {
        close(fd);
        perror("Error mapping file");
        return 0;
    }
    
	jpeg_data_save_data (data, &d, &size);
    
    msync(d, temp_size, MS_SYNC);
    munmap(d, temp_size);
    
    ftruncate(fd, size);
    close(fd);
    
	if (size == 0)
		return 0;

	return 1;
}

void
jpeg_data_save_data (JPEGData *data, unsigned char **d, unsigned int *ds)
{
	unsigned int i, eds = 0;
	JPEGSection s;
	unsigned char *ed = NULL;

	if (!data)
		return;
	if (!d)
		return;
	if (!ds)
		return;

	for (*ds = i = 0; i < data->count; i++) {
		s = data->sections[i];

		/* Write the marker */
        CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + 2));
		(*d)[*ds + 0] = 0xff;
		(*d)[*ds + 1] = s.marker;
		*ds += 2;

		switch (s.marker) {
		case JPEG_MARKER_SOI:
		case JPEG_MARKER_EOI:
			break;
		case JPEG_MARKER_APP1:
            if (s.content.exif != NULL)
            {
                exif_data_save_data (s.content.exif, &ed, &eds);
                if (!ed) break;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + 2));
                (*d)[*ds + 0] = (eds + 2) >> 8;
                (*d)[*ds + 1] = (eds + 2) >> 0;
                *ds += 2;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + eds));
                memcpy (*d + *ds, ed, eds);
                *ds += eds;
                free (ed);
            }
            else if (s.content.xmp != NULL)
            {
                xmp_data_save_data(s.content.xmp, &ed, &eds);
                if (!ed) break;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + 2));
                (*d)[*ds + 0] = (eds + 2) >> 8;
                (*d)[*ds + 1] = (eds + 2) >> 0;
                *ds += 2;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + eds));
                memcpy (*d + *ds, ed, eds);
                *ds += eds;
                free (ed);
            }
            else if (s.content.generic.data != NULL)
            {
                CLEANUP_REALLOC (*d, sizeof (unsigned char) *
                                 (*ds + s.content.generic.size + 2));
                (*d)[*ds + 0] = (s.content.generic.size + 2) >> 8;
                (*d)[*ds + 1] = (s.content.generic.size + 2) >> 0;
                *ds += 2;
                memcpy (*d + *ds, s.content.generic.data,
                        s.content.generic.size);
                *ds += s.content.generic.size;
            }
			break;
        case JPEG_MARKER_APP13:
            if (s.content.app13.iptc != NULL)
            {
                iptc_data_save_data (s.content.app13.iptc,
                                     s.content.app13.additional_data,
                                     s.content.app13.additional_size, &ed, &eds);
                if (!ed) break;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + 2));
                (*d)[*ds + 0] = (eds + 2) >> 8;
                (*d)[*ds + 1] = (eds + 2) >> 0;
                *ds += 2;
                CLEANUP_REALLOC (*d, sizeof (unsigned char) * (*ds + eds));
                memcpy (*d + *ds, ed, eds);
                *ds += eds;
                free (ed);
            }
            else if (s.content.app13.additional_data != NULL)
            {
                CLEANUP_REALLOC (*d, sizeof (unsigned char) *
                                 (*ds + s.content.app13.additional_size + 2));
                (*d)[*ds + 0] = (s.content.app13.additional_size + 2) >> 8;
                (*d)[*ds + 1] = (s.content.app13.additional_size + 2) >> 0;
                *ds += 2;
                memcpy (*d + *ds, s.content.app13.additional_data,
                        s.content.app13.additional_size);
                *ds += s.content.app13.additional_size;
            }
            else if (s.content.generic.data != NULL)
            {
                CLEANUP_REALLOC (*d, sizeof (unsigned char) *
                                 (*ds + s.content.generic.size + 2));
                (*d)[*ds + 0] = (s.content.generic.size + 2) >> 8;
                (*d)[*ds + 1] = (s.content.generic.size + 2) >> 0;
                *ds += 2;
                memcpy (*d + *ds, s.content.generic.data,
                        s.content.generic.size);
                *ds += s.content.generic.size;
            }
            break;
        case JPEG_MARKER_SOS:
            CLEANUP_REALLOC (*d, sizeof (unsigned char) *
                             (*ds + s.content.generic.size + 2));
			(*d)[*ds + 0] = (s.content.generic.size + 2) >> 8;
			(*d)[*ds + 1] = (s.content.generic.size + 2) >> 0;
			*ds += 2;
			memcpy (*d + *ds, s.content.generic.data,
				s.content.generic.size);
			*ds += s.content.generic.size;

            if (data->fileDescriptor == -1) {
                CLEANUP_REALLOC (*d, *ds + data->size);
                memcpy (*d + *ds, data->data, data->size);
            }
            else {
                memcpy (*d + *ds, data->fileData + data->imageOffset, data->size);
            }
			*ds += data->size;
            break;
		default:
            CLEANUP_REALLOC (*d, sizeof (unsigned char) *
                             (*ds + s.content.generic.size + 2));
			(*d)[*ds + 0] = (s.content.generic.size + 2) >> 8;
			(*d)[*ds + 1] = (s.content.generic.size + 2) >> 0;
			*ds += 2;
			memcpy (*d + *ds, s.content.generic.data,
				s.content.generic.size);
			*ds += s.content.generic.size;
			break;
		}
	}
}

JPEGData *
jpeg_data_new_from_data (const unsigned char *d,
			 unsigned int size)
{
	JPEGData *data;

	data = jpeg_data_new ();
	jpeg_data_load_data (data, d, size);
	return (data);
}

void
jpeg_data_load_data (JPEGData *data, const unsigned char *d,
		     unsigned int size)
{
    int iptc_offset;
	unsigned int i, o, len, iptc_len, extra_offset;
	JPEGSection *s;
	JPEGMarker marker;

	if (!data) return;
	if (!d) return;

	for (o = 0; o < size;) {

		/*
		 * JPEG sections start with 0xff. The first byte that is
		 * not 0xff is a marker (hopefully).
		 */
		for (i = 0; i < MIN(7, size - o); i++)
			if (d[o + i] != 0xff)
				break;
		if ((i >= size - o) || !JPEG_IS_MARKER (d[o + i])) {
			exif_log (data->priv->log, EXIF_LOG_CODE_CORRUPT_DATA, "jpeg-data",
					_("Data does not follow JPEG specification."));
			return;
		}
		marker = d[o + i];

		/* Append this section */
		jpeg_data_append_section (data);
		if (!data->count) return;
		s = &data->sections[data->count - 1];
		s->marker = marker;
		o += i + 1;

		switch (s->marker) {
		case JPEG_MARKER_SOI:
		case JPEG_MARKER_EOI:
			break;
		default:

			/* Read the length of the section */
			if (2 > size - o) { o = size; break; }
			len = ((d[o] << 8) | d[o + 1]) - 2;
			if (len > size) { o = size; break; }
			o += 2;
			if (len > size - o) { o = size; break; }

			switch (s->marker) {
			case JPEG_MARKER_APP1:
                if (len > sizeof(ExifHeader) &&
                    memcmp(d + o, ExifHeader, sizeof(ExifHeader)) == 0) {
                    s->content.exif = exif_data_new_from_data (d + o - 4, len + 4);
                    break;
                }
                else if (len > sizeof(XMPHeader) &&
                         memcmp(d + o, XMPHeader, sizeof(XMPHeader)) == 0) {
                    XmpData *xmp = xmp_data_new_from_data (d + o - 4, len + 4);
                    if (xmp->xml != NULL) {
                        s->content.xmp = xmp;
                        break;
                    }
                    if (xmp)
                        xmp_data_unref(xmp);
                }
                s->content.generic.data = malloc (sizeof (unsigned char) * len);
                if (!s->content.generic.data) {
                    EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data", sizeof (char) * len);
                    return;
                }
                s->content.generic.size = len;
                memcpy (s->content.generic.data, &d[o], len);
				break;
            case JPEG_MARKER_APP13:
                iptc_offset = iptc_jpeg_ps3_find_iptc (d + o, len, &iptc_len);
                if (iptc_offset > 0) {
                    s->content.app13.iptc =
                            iptc_data_new_from_data(d + o + iptc_offset, iptc_len);
                    extra_offset = iptc_offset + iptc_len;
                    if (extra_offset % 2) // Take zero padding into account
                        extra_offset++;
                    if (len > extra_offset) {
                        s->content.app13.additional_size = len - extra_offset;
                        s->content.app13.additional_data =
                            malloc (sizeof (unsigned char) * s->content.app13.additional_size);
                        memcpy (s->content.app13.additional_data, &d[o + extra_offset],
                                s->content.app13.additional_size);
                    }
                    break;
                }
                s->content.generic.data = malloc (sizeof (unsigned char) * len);
                if (!s->content.generic.data) {
                    EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data", sizeof (char) * len);
                    return;
                }
                s->content.generic.size = len;
                memcpy (s->content.generic.data, &d[o], len);
                break;
            case JPEG_MARKER_SOS:
                s->content.generic.data =
                    malloc (sizeof (unsigned char) * len);
                if (!s->content.generic.data) {
                    EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data", sizeof (char) * len);
                    return;
                }
                s->content.generic.size = len;
                memcpy (s->content.generic.data, &d[o], len);
                    
                /* Image data */
                data->size = size - o - len;
                if (data->size >= 2) {
                    /* -2 means 'take all but the last 2 bytes which are
                     hoped to be JPEG_MARKER_EOI */
                    data->size -= 2;
                    if (d[o + len + data->size] != 0xFF) {
                        /* A truncated file (i.e. w/o JPEG_MARKER_EOI at the end).
                            Instead of trying to use the last two bytes as marker,
                            touching memory beyond allocated memory and posssibly saving
                            back screwed file, we rather take the rest of the file. */
                        data->size += 2;
                    }
                }
                if (data->fileDescriptor == -1)
                {
                    data->data = malloc (sizeof (unsigned char) * data->size);
                    if (!data->data) {
                        EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data", sizeof (char) * data->size);
                        data->size = 0;
                        return;
                    }
                    memcpy (data->data, d + o + len,
                            data->size);
                }
                data->imageOffset = o + len;
                o += data->size;
                break;
			default:
				s->content.generic.data =
						malloc (sizeof (unsigned char) * len);
				if (!s->content.generic.data) {
					EXIF_LOG_NO_MEMORY (data->priv->log, "jpeg-data", sizeof (char) * len);
					return;
				}
				s->content.generic.size = len;
				memcpy (s->content.generic.data, &d[o], len);
				break;
			}
			o += len;
			break;
		}
	}
}

JPEGData *
jpeg_data_new_from_file (const char *path)
{
    JPEGData *data;
    
    data = jpeg_data_new ();
    jpeg_data_load_file (data, path);
    return (data);
}

void
jpeg_data_load_file (JPEGData *data, const char *path)
{
    if (!data) return;
    if (!path) return;

    if ((data->fileDescriptor = open(path, O_RDONLY, 0)) == -1)
        return;

    FILE *f = fopen (path, "rb");
    if (!f) {
        exif_log (data->priv->log, EXIF_LOG_CODE_CORRUPT_DATA, "jpeg-data",
                  _("Path '%s' invalid."), path);
        return;
    }
    fseek (f, 0, SEEK_END);
    data->fileSize = (unsigned int)ftell (f);
    fseek (f, 0, SEEK_SET);
    fclose(f);

    data->fileData = (unsigned char *)mmap(NULL, data->fileSize, PROT_READ, MAP_SHARED, data->fileDescriptor, 0);
    
    jpeg_data_load_data (data, data->fileData, data->fileSize);
}

void
jpeg_data_ref (JPEGData *data)
{
	if (!data)
		return;

	data->priv->ref_count++;
}

void
jpeg_data_unref (JPEGData *data)
{
	if (!data)
		return;

	if (data->priv) {
		data->priv->ref_count--;
		if (!data->priv->ref_count)
			jpeg_data_free (data);
	}
}

void
jpeg_data_free (JPEGData *data)
{
	unsigned int i;
    JPEGSection s;

	if (!data)
		return;

	if (data->count) {
		for (i = 0; i < data->count; i++) {
            s = data->sections[i];
            if (s.content.exif)
                exif_data_unref (s.content.exif);
            if (s.content.xmp)
                xmp_data_unref (s.content.xmp);
            if (s.content.app13.iptc)
                iptc_data_unref (s.content.app13.iptc);
            if (s.content.app13.additional_data)
                free (s.content.app13.additional_data);
            if (s.content.generic.data)
                free (s.content.generic.data);
		}
		free (data->sections);
	}

	if (data->data)
		free (data->data);

    if (data->fileDescriptor != -1) {
        munmap(data->fileData, data->fileSize);
        close(data->fileDescriptor);
    }

	if (data->priv) {
		if (data->priv->log) {
			exif_log_unref (data->priv->log);
			data->priv->log = NULL;
		}
		free (data->priv);
	}

	free (data);
}

void
jpeg_data_dump (JPEGData *data)
{
	unsigned int i;
	JPEGContent content;
	JPEGMarker marker;

	if (!data)
		return;

	printf ("Dumping JPEG data (%i bytes of data)...\n", data->size);
	for (i = 0; i < data->count; i++) {
		marker = data->sections[i].marker;
		content = data->sections[i].content;
		printf ("Section %i (marker 0x%x - %s):\n", i, marker,
			jpeg_marker_get_name (marker));
		printf ("  Description: %s\n",
			jpeg_marker_get_description (marker));
		switch (marker) {
                case JPEG_MARKER_SOI:
                case JPEG_MARKER_EOI:
			break;
                case JPEG_MARKER_APP1:
            if (content.exif != NULL)
                exif_data_dump (content.exif);
            if (content.xmp != NULL)
                xmp_data_dump (content.xmp);
			break;
                case JPEG_MARKER_APP13:
            iptc_data_dump(content.app13.iptc, 0);
            break;
                default:
			printf ("  Size: %i\n", content.generic.size);
                        printf ("  Unknown content.\n");
                        break;
                }
        }
}

static JPEGSection *
jpeg_data_get_section (JPEGData *data, JPEGMarker marker, JPEGContentType type)
{
	unsigned int i;

	if (!data)
		return (NULL);

	for (i = 0; i < data->count; i++)
        if (data->sections[i].marker == marker) {
            if (type == CONTENT_TYPE_EXIF && data->sections[i].content.exif != NULL)
                return (&data->sections[i]);
            else if (type == CONTENT_TYPE_XMP && data->sections[i].content.xmp != NULL)
                return (&data->sections[i]);
            else if (type == CONTENT_TYPE_IPTC && data->sections[i].content.app13.iptc != NULL)
                return (&data->sections[i]);
        }
	return (NULL);
}

ExifData *
jpeg_data_get_exif_data (JPEGData *data)
{
	JPEGSection *section;

	if (!data)
		return NULL;

	section = jpeg_data_get_section (data, JPEG_MARKER_APP1, CONTENT_TYPE_EXIF);
	if (section) {
        exif_data_ref (section->content.exif);
        return (section->content.exif);
	}

	return (NULL);
}

void
jpeg_data_set_exif_data (JPEGData *data, ExifData *exif_data)
{
	JPEGSection *section;

	if (!data) return;

	section = jpeg_data_get_section (data, JPEG_MARKER_APP1, CONTENT_TYPE_EXIF);
	if (!section) {
        if (exif_data) {
            jpeg_data_append_section (data);
            if (data->count < 2) return;
            memmove (&data->sections[2], &data->sections[1],
                     sizeof (JPEGSection) * (data->count - 2));
            memset (&data->sections[1], 0, sizeof (JPEGSection));
            section = &data->sections[1];
        }
	} else {
		exif_data_unref (section->content.exif);
	}
    if (section) {
        if (exif_data) {
            section->marker = JPEG_MARKER_APP1;
            section->content.exif = exif_data;
            exif_data_ref (exif_data);
        }
        else {
            jpeg_data_remove_section (data, section);
        }
    }
}

IptcData *
jpeg_data_get_iptc_data (JPEGData *data)
{
    JPEGSection *section;
    
    if (!data)
        return NULL;
    
    section = jpeg_data_get_section (data, JPEG_MARKER_APP13, CONTENT_TYPE_IPTC);
    if (section) {
        iptc_data_ref (section->content.app13.iptc);
        return (section->content.app13.iptc);
    }
    
    return (NULL);
}

void
jpeg_data_set_iptc_data (JPEGData *data, IptcData *iptc_data)
{
    JPEGSection *section;
    
    if (!data) return;

    section = jpeg_data_get_section (data, JPEG_MARKER_APP13, CONTENT_TYPE_IPTC);
    if (!section) {
        if (iptc_data) {
            jpeg_data_append_section (data);
            if (data->count < 3) return;
            unsigned int i, idx = 1;
            for (i = 0; i < data->count; i++) {
                if (data->sections[i].marker == JPEG_MARKER_APP1) {
                    if (data->sections[i].content.exif != NULL) {
                        idx = i + 1;
                    }
                    else if (data->sections[i].content.xmp != NULL) {
                        idx = i + 1;
                        break;
                    }
                }
                else if (data->sections[i].marker == JPEG_MARKER_SOF0 ||
                         data->sections[i].marker == JPEG_MARKER_APP14 ||
                         data->sections[i].marker == JPEG_MARKER_APP15) {
                    break;
                }
            }
            memmove (&data->sections[idx + 1], &data->sections[idx],
                     sizeof (JPEGSection) * (data->count - (idx + 1)));
            memset (&data->sections[idx], 0, sizeof (JPEGSection));
            section = &data->sections[idx];
        }
    } else {
        iptc_data_unref (section->content.app13.iptc);
        section->content.app13.iptc = NULL;
    }
    if (section) {
        if (iptc_data) {
            section->marker = JPEG_MARKER_APP13;
            section->content.app13.iptc = iptc_data;
            iptc_data_ref (iptc_data);
        }
        else if (!section->content.app13.additional_data) {
            jpeg_data_remove_section (data, section);
        }
    }
}

XmpData *
jpeg_data_get_xmp_data (JPEGData *data)
{
    JPEGSection *section;
    
    if (!data)
        return NULL;
    
    section = jpeg_data_get_section (data, JPEG_MARKER_APP1, CONTENT_TYPE_XMP);
    if (section) {
        xmp_data_ref (section->content.xmp);
        return (section->content.xmp);
    }
    
    return (NULL);
}

void
jpeg_data_set_xmp_data (JPEGData *data, XmpData *xmp_data)
{
    JPEGSection *section;
    
    if (!data) return;
    
    section = jpeg_data_get_section (data, JPEG_MARKER_APP1, CONTENT_TYPE_XMP);
    if (!section) {
        if (xmp_data) {
            jpeg_data_append_section (data);
            if (data->count < 3) return;
            unsigned int i, idx = 1;
            for (i = 0; i < data->count; i++) {
                if (data->sections[i].marker == JPEG_MARKER_APP1 &&
                    data->sections[i].content.exif != NULL) {
                    idx = i + 1;
                    break;
                }
            }
            memmove (&data->sections[idx + 1], &data->sections[idx],
                     sizeof (JPEGSection) * (data->count - (idx + 1)));
            memset (&data->sections[idx], 0, sizeof (JPEGSection));
            section = &data->sections[idx];
        }
    } else {
        xmp_data_unref (section->content.xmp);
    }
    if (section) {
        if (xmp_data) {
            section->marker = JPEG_MARKER_APP1;
            section->content.xmp = xmp_data;
            xmp_data_ref (xmp_data);
        }
        else {
            jpeg_data_remove_section (data, section);
        }
    }
}

void
jpeg_data_log (JPEGData *data, ExifLog *log)
{
	if (!data || !data->priv) return;
	if (data->priv->log) exif_log_unref (data->priv->log);
	data->priv->log = log;
	exif_log_ref (log);
}
