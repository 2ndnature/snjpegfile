/* jpeg-data.h
 *
 * Copyright � 2001 Lutz M�ller <lutz@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA.
 */

#ifndef __JPEG_DATA_H__
#define __JPEG_DATA_H__

#include "libjpeg/jpeg-marker.h"

#include <libexif/exif-data.h>
#include <libexif/exif-log.h>

#include <libiptcdata/iptc-data.h>
#include "xmp-data.h"

typedef enum {
    CONTENT_TYPE_GENERIC = 0,
    CONTENT_TYPE_EXIF,
    CONTENT_TYPE_XMP,
    CONTENT_TYPE_IPTC,
} JPEGContentType;

typedef ExifData * JPEGContentAPP1Exif;
typedef XmpData  * JPEGContentAPP1Xmp;

typedef struct _JPEGContentAPP13 JPEGContentAPP13;
struct _JPEGContentAPP13
{
    IptcData *iptc;
    unsigned char *additional_data;
    unsigned int additional_size;
};

typedef struct _JPEGContentGeneric JPEGContentGeneric;
struct _JPEGContentGeneric
{
    unsigned char *data;
    unsigned int size;
};

typedef struct _JPEGContent JPEGContent;
struct _JPEGContent
{
	JPEGContentGeneric  generic;
    JPEGContentAPP1Exif exif;
    JPEGContentAPP1Xmp  xmp;
    JPEGContentAPP13    app13;
};

typedef struct _JPEGSection JPEGSection;
struct _JPEGSection
{
	JPEGMarker marker;
	JPEGContent content;
};

typedef struct _JPEGData        JPEGData;
typedef struct _JPEGDataPrivate JPEGDataPrivate;

struct _JPEGData
{
	JPEGSection *sections;
	unsigned int count;

	unsigned char *data;
	unsigned int size;
    
    int fileDescriptor;
    unsigned char *fileData;
    unsigned int fileSize;
    unsigned int imageOffset;

	JPEGDataPrivate *priv;
};

JPEGData *jpeg_data_new           (void);
JPEGData *jpeg_data_new_from_file (const char *path);
JPEGData *jpeg_data_new_from_data (const unsigned char *data,
				   unsigned int size);

void      jpeg_data_ref   (JPEGData *data);
void      jpeg_data_unref (JPEGData *data);
void      jpeg_data_free  (JPEGData *data);

void      jpeg_data_load_data     (JPEGData *data, const unsigned char *d,
				   unsigned int size);
void      jpeg_data_save_data     (JPEGData *data, unsigned char **d,
				   unsigned int *size);

void      jpeg_data_load_file     (JPEGData *data, const char *path);
int       jpeg_data_save_file     (JPEGData *data, const char *path);

void      jpeg_data_set_exif_data (JPEGData *data, ExifData *exif_data);
ExifData *jpeg_data_get_exif_data (JPEGData *data);

void      jpeg_data_set_iptc_data (JPEGData *data, IptcData *iptc_data);
IptcData *jpeg_data_get_iptc_data (JPEGData *data);

void      jpeg_data_set_xmp_data (JPEGData *data, XmpData *xmp_data);
XmpData  *jpeg_data_get_xmp_data (JPEGData *data);

void      jpeg_data_dump (JPEGData *data);

void      jpeg_data_append_section (JPEGData *data);

void      jpeg_data_log (JPEGData *data, ExifLog *log);

#endif /* __JPEG_DATA_H__ */
