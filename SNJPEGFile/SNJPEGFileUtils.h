//
//  SNJPEGFile.m
//
//  Created by Brian Gerfort of 2ndNature on 22/05/2017.
//
//  This code is in the public domain.
//

#import <Foundation/Foundation.h>
#import "libexif/exif-data.h"
#import "libexif/exif-content.h"
#import "libiptcdata/iptc-data.h"
#import "libiptcdata/iptc-dataset.h"
#import <libjpeg/jpeg-data.h>

@interface NSMutableDictionary (SNJPEGFileUtils)

- (void)deeplyAddEntriesFromDictionary:(nonnull NSDictionary *)dictionary removeIfNullClass:(BOOL)removeNulls;

@end

ExifFormat format_for_tag(ExifTag tag, ExifIfd ifd);
ExifEntry * _Nonnull create_tag(ExifData * _Nonnull exif, ExifIfd ifd, ExifTag tag, ExifLong len);
