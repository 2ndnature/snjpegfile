//
//  SNXmpSidecar.h
//
//  Created by Brian Gerfort of 2ndNature on 14/10/2017.
//
//  This code is in the public domain.
//

#import <Foundation/Foundation.h>

@interface SNXmpSidecar : NSObject

@property (nonatomic, assign) NSInteger exifOrientation;
@property (nonatomic, readonly) NSInteger originalOrientation;
@property (nonatomic, readonly) NSMutableDictionary *cameraRawSettings;
@property (nonatomic, readonly) BOOL cameraRawSettingsChanged;

+ (dispatch_queue_t)libXMLQueue;

- (instancetype)initWithOrientation:(NSUInteger)orientation;
- (instancetype)initWithXmpFile:(NSURL *)fileURL;
- (void)addMetadataProperties:(NSDictionary *)metadata;
- (BOOL)saveToFile:(NSURL *)fileURL;

@end
