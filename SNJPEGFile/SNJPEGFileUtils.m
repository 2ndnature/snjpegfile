//
//  SNJPEGFile.m
//
//  Created by Brian Gerfort of 2ndNature on 22/05/2017.
//
//  This code is in the public domain.
//

#import "SNJPEGFileUtils.h"

@implementation NSMutableDictionary (SNJPEGFileUtils)

- (void)deeplyAddEntriesFromDictionary:(nonnull NSDictionary *)dictionary removeIfNullClass:(BOOL)removeNulls
{
    for (id key in dictionary)
    {
        id value = [dictionary objectForKey:key];
        id ourValue = [self objectForKey:key];
        if (ourValue != nil)
        {
            if (removeNulls && [value isKindOfClass:[NSNull class]])
            {
                [self removeObjectForKey:key];
            }
            else if ([ourValue isKindOfClass:[NSDictionary class]])
            {
                if ([value isKindOfClass:[NSDictionary class]])
                {
                    if ([ourValue isKindOfClass:[NSMutableDictionary class]] == NO)
                    {
                        ourValue = CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFPropertyListRef)ourValue, kCFPropertyListMutableContainers));
                        [self setObject:ourValue forKey:key];
                    }
                    [ourValue deeplyAddEntriesFromDictionary:value removeIfNullClass:removeNulls];
                }
                else
                {
                    [self setObject:value forKey:key];
                }
            }
            else
            {
                [self setObject:value forKey:key];
            }
        }
        else if ([value isKindOfClass:[NSNull class]] == NO || removeNulls == NO)
        {
            [self setObject:value forKey:key];
        }
    }
}

@end

ExifFormat format_for_tag(ExifTag tag, ExifIfd ifd)
{
    switch (ifd)
    {
        default:
        case EXIF_IFD_0:
        case EXIF_IFD_1:
        case EXIF_IFD_EXIF:
        {
            switch (tag)
            {
                case EXIF_TAG_CFA_PATTERN:
                case EXIF_TAG_TIFF_EP_STANDARD_ID:
                case EXIF_TAG_XML_PACKET:
                case EXIF_TAG_IMAGE_RESOURCES:
                case EXIF_TAG_XP_TITLE:
                case EXIF_TAG_XP_COMMENT:
                case EXIF_TAG_XP_AUTHOR:
                case EXIF_TAG_XP_KEYWORDS:
                case EXIF_TAG_XP_SUBJECT:
                    return EXIF_FORMAT_BYTE;
                    
                case EXIF_TAG_ORIENTATION:
                case EXIF_TAG_RESOLUTION_UNIT:
                case EXIF_TAG_EXPOSURE_PROGRAM:
                case EXIF_TAG_ISO_SPEED_RATINGS:
                case EXIF_TAG_SUBJECT_AREA:
                case EXIF_TAG_METERING_MODE:
                case EXIF_TAG_FLASH:
                case EXIF_TAG_COMPRESSION:
                case EXIF_TAG_COLOR_SPACE:
                case EXIF_TAG_SENSING_METHOD:
                case EXIF_TAG_EXPOSURE_MODE:
                case EXIF_TAG_WHITE_BALANCE:
                case EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM:
                case EXIF_TAG_SCENE_CAPTURE_TYPE:
                case EXIF_TAG_FOCAL_PLANE_RESOLUTION_UNIT:
                case EXIF_TAG_CUSTOM_RENDERED:
                case EXIF_TAG_RELATED_IMAGE_WIDTH:
                case EXIF_TAG_RELATED_IMAGE_LENGTH:
                case EXIF_TAG_TRANSFER_FUNCTION:
                case EXIF_TAG_CFA_REPEAT_PATTERN_DIM:
                case EXIF_TAG_YCBCR_POSITIONING:
                case EXIF_TAG_YCBCR_SUB_SAMPLING:
                case EXIF_TAG_PLANAR_CONFIGURATION:
                case EXIF_TAG_SAMPLES_PER_PIXEL:
                case EXIF_TAG_PHOTOMETRIC_INTERPRETATION:
                case EXIF_TAG_BITS_PER_SAMPLE:
                case EXIF_TAG_INTEROPERABILITY_VERSION:
                case EXIF_TAG_LIGHT_SOURCE:
                case EXIF_TAG_SPATIAL_FREQUENCY_RESPONSE:
                case EXIF_TAG_SUBJECT_LOCATION:
                case EXIF_TAG_FILL_ORDER:
                case EXIF_TAG_TRANSFER_RANGE:
                case EXIF_TAG_GAIN_CONTROL:
                case EXIF_TAG_CONTRAST:
                case EXIF_TAG_SATURATION:
                case EXIF_TAG_SHARPNESS:
                case EXIF_TAG_SUBJECT_DISTANCE_RANGE:
                case EXIF_TAG_SENSITIVITY_TYPE:
                    return EXIF_FORMAT_SHORT;
                    
                case EXIF_TAG_TIME_ZONE_OFFSET:
                    return EXIF_FORMAT_SSHORT;
                    
                case EXIF_TAG_SUB_IFDS:
                case EXIF_TAG_JPEG_PROC:
                case EXIF_TAG_JPEG_INTERCHANGE_FORMAT:
                case EXIF_TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:
                case EXIF_TAG_NEW_SUBFILE_TYPE:
                case EXIF_TAG_IPTC_NAA:
                case EXIF_TAG_EXIF_IFD_POINTER:
                case EXIF_TAG_GPS_INFO_IFD_POINTER:
                case EXIF_TAG_INTEROPERABILITY_IFD_POINTER:
                case EXIF_TAG_IMAGE_WIDTH:
                case EXIF_TAG_IMAGE_LENGTH:
                case EXIF_TAG_STRIP_OFFSETS:
                case EXIF_TAG_ROWS_PER_STRIP:
                case EXIF_TAG_STRIP_BYTE_COUNTS:
                case EXIF_TAG_PIXEL_X_DIMENSION:
                case EXIF_TAG_PIXEL_Y_DIMENSION:
                case EXIF_TAG_OUTPUT_SENSITIVITY:
                case EXIF_TAG_RECOMMENDED_EXPOSURE_INDEX:
                case EXIF_TAG_ISO_SPEED:
                case EXIF_TAG_ISO_SPEED_LATITUDE_YYY:
                case EXIF_TAG_ISO_SPEED_LATITUDE_ZZZ:
                    return EXIF_FORMAT_LONG;
                    
                case EXIF_TAG_BATTERY_LEVEL:
                case EXIF_TAG_X_RESOLUTION:
                case EXIF_TAG_Y_RESOLUTION:
                case EXIF_TAG_EXPOSURE_TIME:
                case EXIF_TAG_FNUMBER:
                case EXIF_TAG_APERTURE_VALUE:
                case EXIF_TAG_FOCAL_LENGTH:
                case EXIF_TAG_COMPRESSED_BITS_PER_PIXEL:
                case EXIF_TAG_FOCAL_PLANE_X_RESOLUTION:
                case EXIF_TAG_FOCAL_PLANE_Y_RESOLUTION:
                case EXIF_TAG_REFERENCE_BLACK_WHITE:
                case EXIF_TAG_YCBCR_COEFFICIENTS:
                case EXIF_TAG_PRIMARY_CHROMATICITIES:
                case EXIF_TAG_WHITE_POINT:
                case EXIF_TAG_MAX_APERTURE_VALUE:
                case EXIF_TAG_FLASH_ENERGY:
                case EXIF_TAG_EXPOSURE_INDEX:
                case EXIF_TAG_DIGITAL_ZOOM_RATIO:
                case EXIF_TAG_GAMMA:
                case EXIF_TAG_LENS_SPECIFICATION:
                    return EXIF_FORMAT_RATIONAL;
                    
                case EXIF_TAG_BRIGHTNESS_VALUE:
                case EXIF_TAG_EXPOSURE_BIAS_VALUE:
                case EXIF_TAG_SHUTTER_SPEED_VALUE:
                case EXIF_TAG_SUBJECT_DISTANCE:
                    return EXIF_FORMAT_SRATIONAL;
                    
                case EXIF_TAG_INTEROPERABILITY_INDEX:
                case EXIF_TAG_MAKE:
                case EXIF_TAG_MODEL:
                case EXIF_TAG_SOFTWARE:
                case EXIF_TAG_DATE_TIME:
                case EXIF_TAG_DATE_TIME_ORIGINAL:
                case EXIF_TAG_DATE_TIME_DIGITIZED:
                case EXIF_TAG_USER_COMMENT:
                case EXIF_TAG_SUB_SEC_TIME_ORIGINAL:
                case EXIF_TAG_SUB_SEC_TIME_DIGITIZED:
                case EXIF_TAG_ARTIST:
                case EXIF_TAG_IMAGE_DESCRIPTION:
                case EXIF_TAG_DOCUMENT_NAME:
                case EXIF_TAG_COPYRIGHT:
                case EXIF_TAG_SPECTRAL_SENSITIVITY:
                case EXIF_TAG_SUB_SEC_TIME:
                case EXIF_TAG_RELATED_SOUND_FILE:
                case EXIF_TAG_RELATED_IMAGE_FILE_FORMAT:
                case EXIF_TAG_IMAGE_UNIQUE_ID:
                case EXIF_TAG_LENS_MAKE:
                case EXIF_TAG_LENS_MODEL:
                case EXIF_TAG_LENS_SERIAL_NUMBER:
                case EXIF_TAG_CAMERA_OWNER_NAME:
                case EXIF_TAG_BODY_SERIAL_NUMBER:
                    return EXIF_FORMAT_ASCII;
                    
                case EXIF_TAG_INTER_COLOR_PROFILE:
                case EXIF_TAG_OECF:
                case EXIF_TAG_EXIF_VERSION:
                case EXIF_TAG_COMPONENTS_CONFIGURATION:
                case EXIF_TAG_MAKER_NOTE:
                case EXIF_TAG_FLASH_PIX_VERSION:
                case EXIF_TAG_FILE_SOURCE:
                case EXIF_TAG_SCENE_TYPE:
                case EXIF_TAG_NEW_CFA_PATTERN:
                case EXIF_TAG_DEVICE_SETTING_DESCRIPTION:
                case EXIF_TAG_PRINT_IMAGE_MATCHING:
                case EXIF_TAG_PADDING:
                default:
                    return EXIF_FORMAT_UNDEFINED;
            }
            break;
        }
        case EXIF_IFD_GPS:
        {
            switch ((int)tag)
            {
                case EXIF_TAG_GPS_VERSION_ID:
                case EXIF_TAG_GPS_ALTITUDE_REF:
                    return EXIF_FORMAT_BYTE;
                    
                case EXIF_TAG_GPS_DIFFERENTIAL:
                    return EXIF_FORMAT_SHORT;
                
                case EXIF_TAG_GPS_LATITUDE:
                case EXIF_TAG_GPS_LONGITUDE:
                case EXIF_TAG_GPS_ALTITUDE:
                case EXIF_TAG_GPS_TIME_STAMP:
                case EXIF_TAG_GPS_DOP:
                case EXIF_TAG_GPS_SPEED:
                case EXIF_TAG_GPS_TRACK:
                case EXIF_TAG_GPS_IMG_DIRECTION:
                case EXIF_TAG_GPS_DEST_LATITUDE:
                case EXIF_TAG_GPS_DEST_LONGITUDE:
                case EXIF_TAG_GPS_DEST_BEARING:
                case EXIF_TAG_GPS_DEST_DISTANCE:
                case EXIF_TAG_GPS_POSITIONING_ERROR:
                    return EXIF_FORMAT_RATIONAL;
                    
                case EXIF_TAG_GPS_LATITUDE_REF:
                case EXIF_TAG_GPS_LONGITUDE_REF:
                case EXIF_TAG_GPS_SATELLITES:
                case EXIF_TAG_GPS_STATUS:
                case EXIF_TAG_GPS_MEASURE_MODE:
                case EXIF_TAG_GPS_SPEED_REF:
                case EXIF_TAG_GPS_TRACK_REF:
                case EXIF_TAG_GPS_IMG_DIRECTION_REF:
                case EXIF_TAG_GPS_MAP_DATUM:
                case EXIF_TAG_GPS_DEST_LATITUDE_REF:
                case EXIF_TAG_GPS_DEST_LONGITUDE_REF:
                case EXIF_TAG_GPS_DEST_BEARING_REF:
                case EXIF_TAG_GPS_DEST_DISTANCE_REF:
                case EXIF_TAG_GPS_DATE_STAMP:
                    return EXIF_FORMAT_ASCII;

                default:
                case EXIF_TAG_GPS_PROCESSING_METHOD:
                case EXIF_TAG_GPS_AREA_INFORMATION:
                    return EXIF_FORMAT_UNDEFINED;
            }
            break;
        }
    }
}

ExifEntry *create_tag(ExifData *exif, ExifIfd ifd, ExifTag tag, ExifLong len)
{
    void *buf;
    ExifEntry *entry;
    
    /* Create a memory allocator to manage this ExifEntry */
    ExifMem *mem = exif_mem_new_default();
    assert(mem != NULL); /* catch an out of memory condition */
    
    /* Create a new ExifEntry using our allocator */
    entry = exif_entry_new_mem (mem);
    assert(entry != NULL);
    
    /* Allocate memory to use for holding the tag data */
    buf = exif_mem_alloc(mem, len);
    assert(buf != NULL);
    
    /* Fill in the entry */
    entry->data = (unsigned char*)buf;
    entry->size = len;
    entry->tag = tag;
    entry->components = len;
    entry->format = EXIF_FORMAT_UNDEFINED;
    
    /* Attach the ExifEntry to an IFD */
    exif_content_add_entry (exif->ifd[ifd], entry);
    
    /* The ExifMem and ExifEntry are now owned elsewhere */
    exif_mem_unref(mem);
    exif_entry_unref(entry);
    
    return entry;
}
