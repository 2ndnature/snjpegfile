//
//  SNXmpSidecar.m
//
//  Created by Brian Gerfort of 2ndNature on 14/10/2017.
//
//  This code is in the public domain.
//

#import "SNXmpSidecar.h"
#import <ImageIO/ImageIO.h>
#import "libxmpdata/xmp-data.h"

typedef enum : NSUInteger {
    XMPMetadataEntryTypeString,
    XMPMetadataEntryTypeDate,
    XMPMetadataEntryTypeDateTime,
    XMPMetadataEntryTypeAlt,
    XMPMetadataEntryTypeSeq,
    XMPMetadataEntryTypeBag
} XMPMetadataEntryType;

@interface NSString (SNXmpSidecar)

- (BOOL)isInteger;

@end

@implementation NSString (SNXmpSidecar)

- (BOOL)isInteger
{
    if ([self length] == 0) return NO;
    
    for (NSInteger pos = [self length] - 1; pos >= 0; pos--)
    {
        unichar c = [self characterAtIndex:pos];
        if (c < 48 || c > 57)
        {
            return NO;
        }
    }
    
    return YES;
}

@end

@interface XMPNameSpace : NSObject

@property (nonatomic, readonly) NSString *href;
@property (nonatomic, readonly) NSString *prefix;

+ (instancetype)nameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix;

@end

@implementation XMPNameSpace

+ (instancetype)nameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix
{
    return [[XMPNameSpace alloc] initWithWithHref:href prefix:prefix];
}

- (instancetype)initWithWithHref:(NSString *)href prefix:(NSString *)prefix
{
    if ((self = [super init]))
    {
        _href = href;
        _prefix = prefix;
    }
    return self;
}

@end

@interface XMPMetadataEntry : NSObject

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) XMPMetadataEntryType type;
@property (nonatomic, readonly) XMPNameSpace *nameSpace;

+ (instancetype)entryWithName:(NSString *)name type:(XMPMetadataEntryType)type nameSpace:(XMPNameSpace *)nameSpace;

@end

@implementation XMPMetadataEntry

+ (instancetype)entryWithName:(NSString *)name type:(XMPMetadataEntryType)type nameSpace:(XMPNameSpace *)nameSpace
{
    return [[XMPMetadataEntry alloc] initWithName:name type:type nameSpace:nameSpace];
}

- (instancetype)initWithName:(NSString *)name type:(XMPMetadataEntryType)type nameSpace:(XMPNameSpace *)nameSpace
{
    if ((self = [super init]))
    {
        _name = name;
        _type = type;
        _nameSpace = nameSpace;
    }
    return self;
}

@end

@interface SNXmpSidecar ()

@property (nonatomic, copy) NSURL *fileURL;
@property (nonatomic, assign) XmpData *xmpData;
@property (nonatomic, strong) NSDictionary *originalCameraRawSettings;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation SNXmpSidecar

+ (dispatch_queue_t)libXMLQueue
{
    static dispatch_once_t pred;
    static dispatch_queue_t sharedQueue = nil;
    
    dispatch_once(&pred, ^{
        
        sharedQueue = dispatch_queue_create("com.2ndnature.libXMLQueue", DISPATCH_QUEUE_SERIAL);
        
    });
    
    return sharedQueue;
}

- (void)dealloc
{
    if (_xmpData)
    {
        xmp_data_unref(_xmpData);
    }
}

- (instancetype)initWithOrientation:(NSUInteger)orientation
{
    if ((self = [super init]))
    {
        dispatch_sync([SNXmpSidecar libXMLQueue], ^{
            _xmpData = xmp_data_new_xml();
        });
        xmp_data_ref(_xmpData);
        _exifOrientation = orientation; // Making sure originalOrientation will be set to this value
        self.exifOrientation = orientation;
        [self loadXmpValues];
    }
    return self;
}

- (instancetype)initWithXmpData:(XmpData *)xmpData orientation:(NSInteger)orientation
{
    if ((self = [super init]))
    {
        if (xmpData)
        {
            _xmpData = xmpData;
        }
        else
        {
            dispatch_sync([SNXmpSidecar libXMLQueue], ^{
                _xmpData = xmp_data_new_xml();
            });
        }
        xmp_data_ref(_xmpData);
        _originalOrientation = 1;
        self.exifOrientation = orientation;
        [self loadXmpValues];
    }
    return self;
}

- (instancetype)initWithXmpFile:(NSURL *)fileURL
{
    if ((self = [super init]))
    {
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        if (data.length > 10 && [[data subdataWithRange:NSMakeRange(0, 10)] isEqualToData:[@"<x:xmpmeta" dataUsingEncoding:NSUTF8StringEncoding]])
        {
            const char *header = "\xFF\xE1\x01\x7Ehttp://ns.adobe.com/xap/1.0/\0<?xpacket begin=\"\xEF\xBB\xBF\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?> ";
            NSMutableData *mutableData = [NSMutableData dataWithBytes:header length:87];
            [mutableData appendData:data];
            dispatch_sync([SNXmpSidecar libXMLQueue], ^{
                _xmpData = xmp_data_new_from_data(mutableData.bytes, (unsigned int)mutableData.length);
            });
        }
        if (!_xmpData) return nil;
        if (!_xmpData->xml) return nil;
        xmp_data_ref(_xmpData);
        _fileURL = [fileURL copy];
        _exifOrientation = 1;
        _originalOrientation = 1;
        [self loadXmpValues];
    }
    return self;
}

- (void)loadXmpValues
{
    _cameraRawSettings = [NSMutableDictionary dictionaryWithCapacity:200];
    _originalCameraRawSettings = @{};

    if (!_xmpData) return;

    xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:_xmpData preferringXMPCore:NO createIfAbsent:nil];
    if (node)
    {
        xmlChar *value = xmlGetProp(node, (xmlChar *)"OriginalOrientation");
        if (value)
        {
            _originalOrientation = atoi((char *)value);
            xmlFree(value);
        }
        value = xmlGetProp(node, (xmlChar *)"Orientation");
        if (value)
        {
            _exifOrientation = atoi((char *)value);
            xmlFree(value);
        }
        xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/camera-raw-settings/1.0/" prefix:@"crs" inNode:node inXmlDoc:_xmpData->xml];
        if (ns)
        {
            xmlAttr *prop = node->properties;
            while (prop)
            {
                if (prop->ns == ns)
                {
                    xmlChar *value = xmlNodeListGetString(node->doc, prop->children, 1);
                    [_cameraRawSettings setObject:[NSString stringWithUTF8String:(char *)value] forKey:[NSString stringWithUTF8String:(char *)prop->name]];
                    xmlFree(value);
                }
                prop = prop->next;
            }
            xmlNode *childNode = node->children;
            while (childNode != NULL)
            {
                if (childNode->ns == ns)
                {
                    xmlNode *grandChildNode = childNode->children;
                    while (grandChildNode != NULL)
                    {
                        if (strcmp((char *)grandChildNode->name, "Seq") == 0)
                        {
                            NSMutableArray *sequence = [NSMutableArray arrayWithCapacity:10];
                            [_cameraRawSettings setObject:sequence forKey:[NSString stringWithUTF8String:(char *)childNode->name]];
                            xmlNode *greatGrandChildNode = grandChildNode->children;
                            while (greatGrandChildNode != NULL)
                            {
                                if (strcmp((char *)greatGrandChildNode->name, "li") == 0)
                                {
                                    xmlChar *value = xmlNodeListGetString(node->doc, greatGrandChildNode->children, 1);
                                    [sequence addObject:[NSString stringWithUTF8String:(char *)value]];
                                    xmlFree(value);
                                }
                                greatGrandChildNode = greatGrandChildNode->next;
                            }
                        }
                        grandChildNode = grandChildNode->next;
                    }
                }
                childNode = childNode->next;
            }
        }
    }
    _originalCameraRawSettings = CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFPropertyListRef)_cameraRawSettings, kCFPropertyListImmutable));
}

- (NSDateFormatter *)dateFormatter
{
    if (_dateFormatter == nil)
    {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    }
    return _dateFormatter;
}

- (NSDate *)dateFromString:(NSString *)dateString
{
    if ([dateString isKindOfClass:[NSString class]] == NO) return nil;

    NSString *format = nil;
    if (dateString.length == 24) // Date, time, GMT offset
    {
        unichar yearDelimiter = [dateString characterAtIndex:4];
        unichar dateAndTimeDelimiter = [dateString characterAtIndex:10];
        unichar timeDelimiter = [dateString characterAtIndex:13];
        format = [NSString stringWithFormat:@"yyyy%cMM%cdd'%c'HH%cmm%cssZZZZZ", yearDelimiter, yearDelimiter, dateAndTimeDelimiter, timeDelimiter, timeDelimiter];
    }
    else if (dateString.length == 19) // Date and time
    {
        unichar yearDelimiter = [dateString characterAtIndex:4];
        unichar dateAndTimeDelimiter = [dateString characterAtIndex:10];
        unichar timeDelimiter = [dateString characterAtIndex:13];
        format = [NSString stringWithFormat:@"yyyy%cMM%cdd'%c'HH%cmm%css", yearDelimiter, yearDelimiter, dateAndTimeDelimiter, timeDelimiter, timeDelimiter];
    }
    else if (dateString.length == 14 && [dateString isInteger]) // DateTime
    {
        format = @"yyyyMMddHHmmss";
    }
    else if (dateString.length == 10) // Date
    {
        unichar yearDelimiter = [dateString characterAtIndex:4];
        format = [NSString stringWithFormat:@"yyyy%cMM%cdd", yearDelimiter, yearDelimiter];
    }
    else if (dateString.length == 8) // Time or date without delimeters
    {
        if ([dateString isInteger])
        {
            format = @"yyyyMMdd";
        }
        else
        {
            unichar timeDelimiter = [dateString characterAtIndex:2];
            format = [NSString stringWithFormat:@"HH%cmm%css", timeDelimiter, timeDelimiter];
        }
    }
    else
    {
        return nil;
    }
    [self.dateFormatter setDateFormat:format];
    return [self.dateFormatter dateFromString:dateString];
}

- (NSString *)stringFromDate:(NSDate *)date
{
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [self.dateFormatter stringFromDate:date];
}

- (NSString *)stringFromDateTime:(NSDate *)date
{
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    return [self.dateFormatter stringFromDate:date];
}

- (void)setExifOrientation:(NSInteger)exifOrientation
{
    exifOrientation = (exifOrientation > 8) ? 1 : MAX(1, exifOrientation);
    
    xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:_xmpData preferringXMPCore:NO createIfAbsent:[NSDate date]];
    [SNXmpSidecar needNameSpaceWithHref:@"http://ns.2ndnaturesoftware.com/SNJPEGFile/1.0/" prefix:@"snjpegfile" inNode:node inXmlDoc:_xmpData->xml];
    if (!xmlHasProp(node, (xmlChar *)"OriginalOrientation"))
    {
        _originalOrientation = (_exifOrientation) ? _exifOrientation : exifOrientation;
        xmlSetProp(node, (xmlChar *)"snjpegfile:OriginalOrientation", (xmlChar *)[[NSString stringWithFormat:@"%lu", (long)_originalOrientation] UTF8String]);
    }

    _exifOrientation = exifOrientation;
    
    [SNXmpSidecar needNameSpaceWithHref:@"http://ns.adobe.com/tiff/1.0/" prefix:@"tiff" inNode:node inXmlDoc:_xmpData->xml];
    xmlSetProp(node, (xmlChar *)"tiff:Orientation", (xmlChar *)[[NSString stringWithFormat:@"%lu", (long)_exifOrientation] UTF8String]);
}

- (void)addMetadataProperties:(NSDictionary *)metadata
{
    NSDictionary *tiffDictionary = [metadata objectForKey:(id)kCGImagePropertyTIFFDictionary];
    NSString *dateTime = [tiffDictionary objectForKey:(id)kCGImagePropertyTIFFDateTime];
    NSDate *createDate = [self dateFromString:dateTime];
    if (createDate == nil) createDate = [NSDate date];
    
    [self setExifOrientation:([tiffDictionary objectForKey:(id)kCGImagePropertyOrientation]) ? [[tiffDictionary objectForKey:(id)kCGImagePropertyOrientation] integerValue] : 1];

    xmlNode *descriptionNode = [SNXmpSidecar xapDescriptionNodeInData:_xmpData preferringXMPCore:NO createIfAbsent:createDate];
    if (!descriptionNode) return;
    
    XMPNameSpace *photoshopNameSpace = [XMPNameSpace nameSpaceWithHref:@"http://ns.adobe.com/photoshop/1.0/" prefix:@"photoshop"];
    XMPNameSpace *elementsNameSpace = [XMPNameSpace nameSpaceWithHref:@"http://purl.org/dc/elements/1.1/" prefix:@"dc"];
    XMPNameSpace *xmpNameSpace = [XMPNameSpace nameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/" prefix:@"xmp"];

    NSDictionary<NSString *, NSArray<XMPMetadataEntry *> *> *iptcValues = @{
                                 (id)kCGImagePropertyIPTCSpecialInstructions:@[[XMPMetadataEntry entryWithName:@"Instructions" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace]],
                                 (id)kCGImagePropertyIPTCCategory:@[[XMPMetadataEntry entryWithName:@"Category" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace]],
                                 (id)kCGImagePropertyIPTCUrgency:@[[XMPMetadataEntry entryWithName:@"Urgency" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace]],
                                 (id)kCGImagePropertyIPTCHeadline:@[[XMPMetadataEntry entryWithName:@"Headline" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace],[XMPMetadataEntry entryWithName:@"title" type:XMPMetadataEntryTypeAlt nameSpace:elementsNameSpace]],
                                 (id)kCGImagePropertyIPTCCredit:@[[XMPMetadataEntry entryWithName:@"Credit" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace]],
                                 (id)kCGImagePropertyIPTCOriginalTransmissionReference:@[[XMPMetadataEntry entryWithName:@"TransmissionReference" type:XMPMetadataEntryTypeString nameSpace:photoshopNameSpace]],
                                 (id)kCGImagePropertyIPTCDateCreated:@[[XMPMetadataEntry entryWithName:@"DateCreated" type:XMPMetadataEntryTypeDateTime nameSpace:photoshopNameSpace], [XMPMetadataEntry entryWithName:@"CreateDate" type:XMPMetadataEntryTypeDateTime nameSpace:xmpNameSpace]],
                                 (id)kCGImagePropertyIPTCCaptionAbstract:@[[XMPMetadataEntry entryWithName:@"description" type:XMPMetadataEntryTypeAlt nameSpace:elementsNameSpace]],
                                 (id)kCGImagePropertyIPTCByline:@[[XMPMetadataEntry entryWithName:@"creator" type:XMPMetadataEntryTypeSeq nameSpace:elementsNameSpace]],
                                 (id)kCGImagePropertyIPTCKeywords:@[[XMPMetadataEntry entryWithName:@"subject" type:XMPMetadataEntryTypeBag nameSpace:elementsNameSpace]],
                                 (id)kCGImagePropertyIPTCCopyrightNotice:@[[XMPMetadataEntry entryWithName:@"rights" type:XMPMetadataEntryTypeAlt nameSpace:elementsNameSpace]],
                                 (id)kCGImagePropertyIPTCCopyrightNotice:@[[XMPMetadataEntry entryWithName:@"rights" type:XMPMetadataEntryTypeAlt nameSpace:elementsNameSpace]],
                                 };
    [iptcValues enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, NSArray<XMPMetadataEntry *> *entries, BOOL * _Nonnull stop) {
        
        id value = [[metadata objectForKey:(id)kCGImagePropertyIPTCDictionary] objectForKey:key];
        [entries enumerateObjectsUsingBlock:^(XMPMetadataEntry * _Nonnull entry, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if (([value isKindOfClass:[NSString class]] && [(NSString *)value length] > 0) ||
                ([value isKindOfClass:[NSArray class]] && [(NSArray *)value count] > 0))
            {
                xmlNsPtr nameSpace = [SNXmpSidecar needNameSpaceWithHref:entry.nameSpace.href prefix:entry.nameSpace.prefix inNode:descriptionNode inXmlDoc:_xmpData->xml];
                switch (entry.type)
                {
                    case XMPMetadataEntryTypeString:
                        xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"%@:%@", entry.nameSpace.prefix, entry.name] UTF8String], (xmlChar *)[value UTF8String]);
                        break;
                    case XMPMetadataEntryTypeDate:
                        xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"%@:%@", entry.nameSpace.prefix, entry.name] UTF8String], (xmlChar *)[[self stringFromDate:[self dateFromString:value]] UTF8String]);
                        break;
                    case XMPMetadataEntryTypeDateTime:
                    {
                        if ([key isEqual:(id)kCGImagePropertyIPTCDateCreated])
                        {
                            NSString *timeCreated = [[metadata objectForKey:(id)kCGImagePropertyIPTCDictionary] objectForKey:(id)kCGImagePropertyIPTCTimeCreated];
                            if (timeCreated.length == 6 && [(NSString *)value length] == 8)
                            {
                                xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"%@:%@", entry.nameSpace.prefix, entry.name] UTF8String], (xmlChar *)[[self stringFromDateTime:[self dateFromString:[NSString stringWithFormat:@"%@%@", value, timeCreated]]] UTF8String]);
                            }
                            else // Fall back to just date
                            {
                                xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"%@:%@", entry.nameSpace.prefix, entry.name] UTF8String], (xmlChar *)[[self stringFromDate:[self dateFromString:value]] UTF8String]);
                            }
                        }
                        else
                        {
                            xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"%@:%@", entry.nameSpace.prefix, entry.name] UTF8String], (xmlChar *)[[self stringFromDateTime:[self dateFromString:value]] UTF8String]);
                        }
                        break;
                    }
                    case XMPMetadataEntryTypeAlt:
                    case XMPMetadataEntryTypeSeq:
                    case XMPMetadataEntryTypeBag:
                    {
                        NSString *arrayType;
                        switch (entry.type)
                        {
                            default:
                            case XMPMetadataEntryTypeAlt:
                                arrayType = @"Alt";
                                break;
                            case XMPMetadataEntryTypeSeq:
                                arrayType = @"Seq";
                                break;
                            case XMPMetadataEntryTypeBag:
                                arrayType = @"Bag";
                                break;
                        }
                        if ([value isKindOfClass:[NSArray class]])
                        {
                            xmlNode *typeNode = [SNXmpSidecar needNodeWithName:arrayType inNode:[SNXmpSidecar needNodeWithName:entry.name inNode:descriptionNode nameSpace:nameSpace] nameSpace:descriptionNode->ns];
                            for (NSString *arrayValue in (NSArray *)value)
                            {
                                xmlNode *liNode = xmlNewNode(descriptionNode->ns, (xmlChar *)[@"li" UTF8String]);
                                xmlAddChild(typeNode, liNode);
                                if ([arrayType isEqualToString:@"Alt"])
                                {
                                    xmlSetProp(liNode, (xmlChar *)"xml:lang", (xmlChar *)"x-default");
                                }
                                xmlNodeSetContent(liNode, (xmlChar *)[[arrayValue description] UTF8String]);
                            }
                        }
                        else
                        {
                            xmlNode *liNode = [SNXmpSidecar needNodeWithName:@"li" inNode:[SNXmpSidecar needNodeWithName:arrayType inNode:[SNXmpSidecar needNodeWithName:entry.name inNode:descriptionNode nameSpace:nameSpace] nameSpace:descriptionNode->ns] nameSpace:descriptionNode->ns];
                            if ([arrayType isEqualToString:@"Alt"])
                            {
                                xmlSetProp(liNode, (xmlChar *)"xml:lang", (xmlChar *)"x-default");
                            }
                            xmlNodeSetContent(liNode, (xmlChar *)[[value description] UTF8String]);
                        }
                        break;
                    }
                }
            }
            else // Remove
            {
                xmlNsPtr nameSpace = [SNXmpSidecar maybeNameSpaceWithHref:entry.nameSpace.href prefix:entry.nameSpace.prefix inNode:descriptionNode inXmlDoc:_xmpData->xml];
                if (nameSpace)
                {
                    switch (entry.type)
                    {
                        case XMPMetadataEntryTypeString:
                        case XMPMetadataEntryTypeDate:
                        case XMPMetadataEntryTypeDateTime:
                        {
                            xmlAttrPtr prop = xmlHasProp(descriptionNode, (xmlChar *)[entry.name UTF8String]);
                            if (prop) xmlRemoveProp(prop);
                            break;
                        }
                        case XMPMetadataEntryTypeAlt:
                        case XMPMetadataEntryTypeSeq:
                        case XMPMetadataEntryTypeBag:
                        {
                            xmlNode *node = [SNXmpSidecar maybeNodeWithName:entry.name inNode:descriptionNode nameSpace:nameSpace];
                            if (node) xmlUnlinkNode(node);
                            break;
                        }
                    }
                }
            }
        }];
    }];
    //NSLog(@"desc: %@", self.description);
}

- (BOOL)cameraRawSettingsChanged
{
    return ([self.originalCameraRawSettings isEqualToDictionary:self.cameraRawSettings] == NO);
}

- (void)commitChanges
{
    if (self.originalCameraRawSettings.count > 0)
    {
        [SNXmpSidecar removeNameSpaceWithHref:@"http://ns.adobe.com/camera-raw-settings/1.0/" prefix:@"crs" fromData:self.xmpData];
    }
    xmlNode *descriptionNode = [SNXmpSidecar xapDescriptionNodeInData:_xmpData preferringXMPCore:NO createIfAbsent:[NSDate date]];
    xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/camera-raw-settings/1.0/" prefix:@"crs" inNode:descriptionNode inXmlDoc:_xmpData->xml];
    if (self.cameraRawSettings.count > 0 && !ns)
    {
        [SNXmpSidecar needNameSpaceWithHref:@"http://ns.adobe.com/camera-raw-settings/1.0/" prefix:@"crs" inNode:descriptionNode inXmlDoc:_xmpData->xml];
    }
    [self.cameraRawSettings enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull value, BOOL * _Nonnull stop) {
        
        if ([value isKindOfClass:[NSArray class]])
        {
            xmlNode *seqNode = [SNXmpSidecar needNodeWithName:@"Seq" inNode:[SNXmpSidecar needNodeWithName:key inNode:descriptionNode nameSpace:ns] nameSpace:descriptionNode->ns];
            xmlNode *liNode;
            for (NSString *string in (NSArray *)value)
            {
                liNode = xmlNewNode(descriptionNode->ns, (xmlChar *)"li");
                xmlAddChild(seqNode, liNode);
                xmlNodeSetContent(liNode, (xmlChar *)[(NSString *)string UTF8String]);
            }
        }
        else
        {
            xmlSetProp(descriptionNode, (xmlChar *)[[NSString stringWithFormat:@"crs:%@", key] UTF8String], ([value isKindOfClass:[NSString class]]) ? (xmlChar *)[value UTF8String] : (xmlChar *)[[NSString stringWithFormat:@"%@", value] UTF8String]);
        }
    }];
}

- (BOOL)saveToFile:(NSURL *)fileURL
{
    if ([fileURL isKindOfClass:[NSURL class]] == NO) return NO;

    NSString *xmpString = self.description;
    
    return [xmpString writeToURL:fileURL atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (NSString *)description
{
    [self commitChanges];
    xmlChar *str = NULL;
    int size = 0;
    xmlDocDumpMemoryEnc(_xmpData->xml, &str, &size, "UTF-8");
    NSString *xmlDocStr = [[NSString alloc] initWithBytes:str length:size encoding:NSUTF8StringEncoding];
    return [[[xmlDocStr substringFromIndex:[xmlDocStr rangeOfString:@"<x:xmpmeta "].location] stringByReplacingOccurrencesOfString:@"<?xpacket end=\"w\"?>" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark libxml

+ (BOOL)removeNameSpaceWithHref:(NSString *)nameSpace prefix:(NSString *)prefix fromData:(XmpData *)xmpData
{
    if (!nameSpace || !prefix || !xmpData) return NO;
    xmlNode *descriptionNode = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:NO createIfAbsent:nil];
    xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:nameSpace prefix:prefix inNode:descriptionNode inXmlDoc:xmpData->xml];
    if (ns)
    {
        xmlAttr *prop = descriptionNode->properties;
        while (prop)
        {
            if (prop->ns == ns)
            {
                xmlRemoveProp(prop);
            }
            prop = prop->next;
        }
        xmlNode *childNode = descriptionNode->children;
        xmlNode *unlinkNode = NULL;
        while (childNode != NULL)
        {
            if (childNode->ns == ns)
            {
                unlinkNode = childNode;
            }
            childNode = childNode->next;
            if (unlinkNode)
            {
                xmlUnlinkNode(unlinkNode);
                unlinkNode = NULL;
            }
        }
        return YES;
    }
    return NO;
}

+ (xmlNode *)xapDescriptionNodeInData:(XmpData *)xmpData preferringXMPCore:(BOOL)preferringXMPCore createIfAbsent:(NSDate *)createDate
{
    // createDate should be the date the picture was taken
    if (!xmpData) return NULL;
    short i;
    xmlNode *tempNode;
    xmlNode *xmpmetaNode;
    xmlNode *rdfNode;
    xmlNode *resultNode;
    xmlNs **nsListPtr;
    xmlNs *ns;
    
    //xmlDocDump(stdout, xmpData->xml);
    
    xmpmetaNode = xmpData->xml->children;
    while (xmpmetaNode != NULL)
    {
        if (strcmp((char *)xmpmetaNode->name, "xmpmeta") == 0 && xmpmetaNode->nsDef != NULL && strcmp((char *)xmpmetaNode->nsDef->prefix, "x") == 0)
        {
            break;
        }
        xmpmetaNode = xmpmetaNode->next;
    }
    if (!xmpmetaNode && createDate)
    {
        xmpmetaNode = xmlNewNode(NULL, (xmlChar *)"xmpmeta");
        xmlNsPtr ns = xmlNewNs(xmpmetaNode, (xmlChar *)"adobe:ns:meta/", (xmlChar *)"x");
        xmlSetNs(xmpmetaNode, ns);
        xmlAddPrevSibling(xmpData->xml->last, xmpmetaNode);
        xmlSetProp(xmpmetaNode, (xmlChar *)"x:xmptk", (xmlChar *)"XMP Core 5.4.0");
    }
    
    rdfNode = (xmpmetaNode) ? xmpmetaNode->children : NULL;
    while (rdfNode != NULL)
    {
        if (strcmp((char *)rdfNode->name, "RDF") == 0 && rdfNode->nsDef != NULL && strcmp((char *)rdfNode->nsDef->prefix, "rdf") == 0)
        {
            break;
        }
        rdfNode = rdfNode->next;
    }
    if (!rdfNode && createDate)
    {
        rdfNode = xmlNewNode(NULL, (xmlChar *)"RDF");
        xmlNs *ns = xmlNewNs(rdfNode, (xmlChar *)"http://www.w3.org/1999/02/22-rdf-syntax-ns#", (xmlChar *)"rdf");
        xmlSetNs(rdfNode, ns);
        xmlAddChild(xmpmetaNode, rdfNode);
    }
    
    // There can be multiple rdf:Description nodes.
    // Prefer the one that already has an xmp or Iptc4xmpCore namespace
    // or alternatively end up selecting the first occurrence.
    resultNode = NULL;
    tempNode = (rdfNode) ? rdfNode->last : NULL;
    while (tempNode != NULL)
    {
        if (strcmp((char *)tempNode->name, "Description") == 0)
        {
            resultNode = tempNode;
            nsListPtr = xmlGetNsList(xmpData->xml, tempNode);
            if (nsListPtr)
            {
                for (i = 0; nsListPtr[i] != NULL; i++) {
                    ns = nsListPtr[i];
                    if (ns->prefix != NULL && (strcmp((char *)ns->prefix, "xmp") == 0 || (preferringXMPCore && strcmp((char *)ns->prefix, "Iptc4xmpCore") == 0)))
                    {
                        tempNode = NULL;
                        break;
                    }
                }
                xmlFree(nsListPtr);
            }
        }
        tempNode = (tempNode) ? tempNode->prev : NULL;
    }
    
    if (!resultNode && rdfNode && createDate)
    {
        resultNode = xmlNewNode(rdfNode->ns, (xmlChar *)"Description");
        xmlAddChild(rdfNode, resultNode);
        xmlSetProp(resultNode, (xmlChar *)"rdf:about", (xmlChar *)"");
        xmlNewNs(resultNode, (xmlChar *)"http://ns.adobe.com/xap/1.0/", (xmlChar *)"xmp");
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setFormatterBehavior:NSDateFormatterBehavior10_4];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        xmlSetProp(resultNode, (xmlChar *)"xmp:CreateDate", (xmlChar *)[[df stringFromDate:createDate] UTF8String]);
    }
    
    return resultNode;
}

+ (xmlNsPtr)nameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc createIfAbsent:(BOOL)create
{
    short i;
    xmlNs *ns;
    xmlNsPtr result = NULL;
    const char *hrefStr = href.UTF8String;
    const char *prefixStr = prefix.UTF8String;
    xmlNs **nsListPtr = xmlGetNsList(doc, node);
    if (nsListPtr)
    {
        for (i = 0; nsListPtr[i] != NULL; i++) {
            ns = nsListPtr[i];
            if (ns->href != NULL && strcmp((char *)ns->href, hrefStr) == 0 &&
                ns->prefix != NULL && strcmp((char *)ns->prefix, prefixStr) == 0)
            {
                result = ns;
                break;
            }
        }
        xmlFree(nsListPtr);
    }
    if (!result && create)
    {
        result = xmlNewNs(node, (xmlChar *)hrefStr, (xmlChar *)prefixStr);
    }
    return result;
}

+ (xmlNode *)nodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace createIfAbsent:(BOOL)create
{
    if (!node) return NULL;
    xmlNode *resultNode = NULL, *searchNode = NULL;
    const char *nameStr = name.UTF8String;
    searchNode = node->children;
    while (searchNode != NULL)
    {
        if (searchNode->name != NULL && strcmp((char *)searchNode->name, nameStr) == 0)
        {
            resultNode = searchNode;
            break;
        }
        searchNode = searchNode->next;
    }
    if (!resultNode && nameSpace != NULL && create)
    {
        resultNode = xmlNewNode(nameSpace, (xmlChar *)nameStr);
        xmlAddChild(node, resultNode);
    }
    return resultNode;
}

+ (xmlNode *)needNodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace
{
    return [SNXmpSidecar nodeWithName:name inNode:node nameSpace:nameSpace createIfAbsent:YES];
}

+ (xmlNode *)maybeNodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace
{
    return [SNXmpSidecar nodeWithName:name inNode:node nameSpace:nameSpace createIfAbsent:NO];
}

+ (xmlNsPtr)needNameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc
{
    return [SNXmpSidecar nameSpaceWithHref:href prefix:prefix inNode:node inXmlDoc:doc createIfAbsent:YES];
}

+ (xmlNsPtr)maybeNameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc
{
    return [SNXmpSidecar nameSpaceWithHref:href prefix:prefix inNode:node inXmlDoc:doc createIfAbsent:NO];
}

@end

