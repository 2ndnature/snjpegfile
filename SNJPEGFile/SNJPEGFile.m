//
//  SNJPEGFile.m
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain.
//

#import "SNJPEGFile.h"
#import "SNJPEGFileUtils.h"

#if DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

#if 0
#   define VLog(fmt, ...) NSLog((fmt), ##__VA_ARGS__);
#else
#   define VLog(...)
#endif

@interface SNXmpSidecar (LibXMLConvenienceMethods)

@property (nonatomic, readonly) XmpData *xmpData;

- (instancetype)initWithXmpData:(XmpData *)xmpData orientation:(NSInteger)orientation;
- (void)commitChanges;

+ (BOOL)removeNameSpaceWithHref:(NSString *)nameSpace prefix:(NSString *)prefix fromData:(XmpData *)xmpData;
+ (xmlNsPtr)nameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc createIfAbsent:(BOOL)create;
+ (xmlNode *)xapDescriptionNodeInData:(XmpData *)xmpData preferringXMPCore:(BOOL)preferringXMPCore createIfAbsent:(NSDate *)createDate;
+ (xmlNode *)nodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace createIfAbsent:(BOOL)create;
+ (xmlNode *)needNodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace;
+ (xmlNode *)maybeNodeWithName:(NSString *)name inNode:(xmlNode *)node nameSpace:(xmlNsPtr)nameSpace;
+ (xmlNsPtr)needNameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc;
+ (xmlNsPtr)maybeNameSpaceWithHref:(NSString *)href prefix:(NSString *)prefix inNode:(xmlNode *)node inXmlDoc:(xmlDoc *)doc;

@end

@interface SNJPEGFile ()

@property (nonatomic, strong) SNXmpSidecar *xmpSidecar;
@property (nonatomic, strong) NSDictionary *originalMetadata;
@property (nonatomic, strong) NSMutableDictionary *mutableMetadata;
@property (nonatomic, assign) JPEGData *jpegData;

@end

@implementation SNJPEGFile

- (nullable instancetype)initWithFileURL:(nonnull NSURL *)fileURL
{
    if ([fileURL isKindOfClass:[NSURL class]] == NO || [[NSFileManager defaultManager] fileExistsAtPath:fileURL.path] == NO)
    {
        return nil;
    }
    
    NSData *sample = nil;
    NSFileHandle *fh = [NSFileHandle fileHandleForReadingAtPath:fileURL.path];
    [fh seekToEndOfFile];
    if (fh.offsetInFile > 2)
    {
        [fh seekToFileOffset:0];
        sample = [fh readDataOfLength:2];
    }
    [fh closeFile];
    UInt16 soi = 0xD8FF;
    if ([sample isEqualToData:[NSData dataWithBytes:&soi length:2]] == NO)
    {
        return nil;
    }
    
    if ((self = [super init]))
    {
        __block JPEGData *jpegData;
        dispatch_sync([SNXmpSidecar libXMLQueue], ^{
            jpegData = jpeg_data_new_from_file(fileURL.path.UTF8String);
        });
        if (!jpegData) return nil;
        
        _fileURL = [fileURL copy];
        self.jpegData = jpegData;
        
        [self parseMetadata];
    }
    return self;
}

- (nullable instancetype)initWithData:(nonnull NSData *)data
{
    if ([data isKindOfClass:[NSData class]] == NO) return nil;
    
    if ((self = [super init]))
    {
        JPEGData *jpegData = jpeg_data_new_from_data(data.bytes, (unsigned int)data.length);
        if (!jpegData) return nil;
        
        self.jpegData = jpegData;
        
        [self parseMetadata];
    }
    return self;
}

- (void)dealloc
{
    if (_jpegData)
    {
        jpeg_data_unref(self.jpegData);
        self.jpegData = nil;
    }
}

#pragma mark - Setters and Getters

#if TARGET_OS_IPHONE
- (UIImage *)thumbnail
{
    UIImage *thumbnail = nil;
    ExifData *exifData = jpeg_data_get_exif_data(self.jpegData);
    if (exifData)
    {
        NSData *thumbnailData = nil;
        ExifContent *content;
        ExifEntry *entry;
        
        content = exifData->ifd[EXIF_IFD_EXIF];
        entry = exif_content_get_entry(content, EXIF_TAG_MAKER_NOTE);
        if (entry && entry->size > 2048)
        {
            NSData *makerNote = [NSData dataWithBytes:entry->data length:entry->size];
            UInt8 soi[4] = {0xff,0xd8,0xff,0xdb};
            NSRange startOfImage = [makerNote rangeOfData:[NSData dataWithBytes:&soi length:4] options:0 range:NSMakeRange(0, makerNote.length)];
            if (startOfImage.location != NSNotFound)
            {
                UInt8 eoi[2] = {0xff,0xd9};
                NSRange endOfImage = [makerNote rangeOfData:[NSData dataWithBytes:&eoi length:2] options:0 range:NSMakeRange(startOfImage.location, makerNote.length - startOfImage.location)];
                if (endOfImage.location != NSNotFound)
                {
                    thumbnailData = [makerNote subdataWithRange:NSMakeRange(startOfImage.location, (endOfImage.location + endOfImage.length) - startOfImage.location)];
                }
            }
        }
        
        if (!thumbnailData && exifData->data && exifData->size > 0)
        {
            thumbnailData = [NSData dataWithBytes:exifData->data length:exifData->size];
        }
        
        if (thumbnailData)
        {
            thumbnail = [UIImage imageWithData:thumbnailData];
            if (thumbnail)
            {
                ExifByteOrder exifByteOrder;
                ExifShort imageOrientation = 1;
                ExifShort imageWidth = 0;
                ExifShort imageHeight = 0;
                
                content = exifData->ifd[EXIF_IFD_0];
                entry = exif_content_get_entry(content, EXIF_TAG_ORIENTATION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageOrientation = exif_get_short(entry->data, exifByteOrder);
                }
                
                content = exifData->ifd[EXIF_IFD_EXIF];
                entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_X_DIMENSION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageWidth = exif_get_short(entry->data, exifByteOrder);
                }
                entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_Y_DIMENSION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageHeight = exif_get_short(entry->data, exifByteOrder);
                }
                
                // Remove the black bars, in case the original aspect ratio doesn't match the default 160x120 thumbnail dimensions
                if (imageWidth > 0 && imageHeight > 0)
                {
                    CGSize thumbnailSize = thumbnail.size;
                    if (imageWidth / (CGFloat)imageHeight != thumbnailSize.width / thumbnailSize.height)
                    {
                        CGSize canvas = thumbnailSize;
                        CGRect cropRect = CGRectZero;
                        cropRect.size = thumbnailSize;
                        if (imageWidth > imageHeight)
                        {
                            canvas.height = floorf(thumbnailSize.width * (imageHeight / (CGFloat)imageWidth));
                            cropRect.origin.y = -((thumbnailSize.height - canvas.height) / 2.0);
                        }
                        else
                        {
                            canvas.width = floorf(thumbnailSize.height * (imageWidth / (CGFloat)imageHeight));
                            cropRect.origin.x = -((thumbnailSize.width - canvas.width) / 2.0);
                        }
                        UIGraphicsBeginImageContext(canvas);
                        [thumbnail drawInRect:cropRect];
                        UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        if (croppedImage)
                        {
                            thumbnail = croppedImage;
                        }
                    }
                }
                // Apply orientation transform
                if (imageOrientation != 1)
                {
                    CGFloat degrees;
                    switch (imageOrientation)
                    {
                        case 3:
                        case 4:
                            degrees = 180;
                            break;
                        case 6:
                            degrees = 90;
                            break;
                        case 5:
                        case 7:
                        case 8:
                            degrees = -90;
                            break;
                        default:
                            degrees = 0;
                            break;
                    }
                    CGSize rotatedSize = (degrees == 90 || degrees == -90) ? CGSizeMake(thumbnail.size.height, thumbnail.size.width) : thumbnail.size;
                    UIGraphicsBeginImageContext(rotatedSize);
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    CGContextTranslateCTM(context, rotatedSize.width / 2, rotatedSize.height / 2);
                    CGContextRotateCTM(context, degrees * M_PI / 180);
                    switch (imageOrientation)
                    {
                        case 2:
                        case 4:
                        case 5:
                            CGContextScaleCTM(context, -1.0, -1.0);
                            break;
                        case 7:
                            CGContextScaleCTM(context, 1.0, 1.0);
                            break;
                        default:
                            CGContextScaleCTM(context, 1.0, -1.0);
                            break;
                    }
                    CGContextDrawImage(context, CGRectMake(-thumbnail.size.width / 2, -thumbnail.size.height / 2, thumbnail.size.width, thumbnail.size.height), thumbnail.CGImage);
                    UIImage *properOrientation = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    if (properOrientation)
                    {
                        thumbnail = properOrientation;
                    }
                }
            }
        }
        exif_data_unref(exifData);
    }
    return thumbnail;
}
#else
- (NSImage *)thumbnail
{
    NSImage *thumbnail = nil;
    ExifData *exifData = jpeg_data_get_exif_data(self.jpegData);
    if (exifData)
    {
        NSData *thumbnailData = nil;
        ExifContent *content;
        ExifEntry *entry;

        content = exifData->ifd[EXIF_IFD_EXIF];
        entry = exif_content_get_entry(content, EXIF_TAG_MAKER_NOTE);
        if (entry && entry->size > 2048)
        {
            NSData *makerNote = [NSData dataWithBytes:entry->data length:entry->size];
            UInt8 soi[4] = {0xff,0xd8,0xff,0xdb};
            NSRange startOfImage = [makerNote rangeOfData:[NSData dataWithBytes:&soi length:4] options:0 range:NSMakeRange(0, makerNote.length)];
            if (startOfImage.location != NSNotFound)
            {
                UInt8 eoi[2] = {0xff,0xd9};
                NSRange endOfImage = [makerNote rangeOfData:[NSData dataWithBytes:&eoi length:2] options:0 range:NSMakeRange(startOfImage.location, makerNote.length - startOfImage.location)];
                if (endOfImage.location != NSNotFound)
                {
                    thumbnailData = [makerNote subdataWithRange:NSMakeRange(startOfImage.location, (endOfImage.location + endOfImage.length) - startOfImage.location)];
                }
            }
        }
        
        if (!thumbnailData && exifData->data && exifData->size > 0)
        {
            thumbnailData = [NSData dataWithBytes:exifData->data length:exifData->size];
        }

        if (thumbnailData)
        {
            thumbnail = [[NSImage alloc] initWithData:thumbnailData];
            if (thumbnail)
            {
                ExifByteOrder exifByteOrder;
                ExifShort imageOrientation = 1;
                ExifShort imageWidth = 0;
                ExifShort imageHeight = 0;
                
                content = exifData->ifd[EXIF_IFD_0];
                entry = exif_content_get_entry(content, EXIF_TAG_ORIENTATION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageOrientation = exif_get_short(entry->data, exifByteOrder);
                }
                
                content = exifData->ifd[EXIF_IFD_EXIF];
                entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_X_DIMENSION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageWidth = exif_get_short(entry->data, exifByteOrder);
                }
                entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_Y_DIMENSION);
                if (entry)
                {
                    exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                    imageHeight = exif_get_short(entry->data, exifByteOrder);
                }
                
                // Remove the black bars, in case the original aspect ratio doesn't match the default 160x120 thumbnail dimensions
                if (imageWidth > 0 && imageHeight > 0)
                {
                    NSSize thumbnailSize = thumbnail.size;
                    if (imageWidth / (CGFloat)imageHeight != thumbnailSize.width / thumbnailSize.height)
                    {
                        NSSize canvas = thumbnailSize;
                        CGRect cropRect = CGRectZero;
                        cropRect.size = thumbnailSize;
                        if (imageWidth > imageHeight)
                        {
                            canvas.height = floorf(thumbnailSize.width * (imageHeight / (CGFloat)imageWidth));
                            cropRect.origin.y = -((thumbnailSize.height - canvas.height) / 2.0);
                        }
                        else
                        {
                            canvas.width = floorf(thumbnailSize.height * (imageWidth / (CGFloat)imageHeight));
                            cropRect.origin.x = -((thumbnailSize.width - canvas.width) / 2.0);
                        }
                        NSImage *croppedImage = [[NSImage alloc] initWithSize:canvas];
                        [croppedImage lockFocus];
                        [thumbnail drawInRect:cropRect];
                        [croppedImage unlockFocus];
                        if (croppedImage)
                        {
                            thumbnail = croppedImage;
                        }
                    }
                }

                // Apply orientation transform
                if (imageOrientation != 1)
                {
                    CGFloat degrees = 0;
                    switch (imageOrientation)
                    {
                        case 3:
                        case 4:
                            degrees = 180;
                            break;
                        case 5:
                        case 8:
                            degrees = 90;
                            break;
                        case 7:
                        case 6:
                            degrees = -90;
                            break;
                        default:
                            break;
                    }
                    NSSize rotatedSize = (degrees == 90 || degrees == -90) ? NSMakeSize(thumbnail.size.height, thumbnail.size.width) : thumbnail.size;
                    NSAffineTransform *transform = [NSAffineTransform transform];
                    [transform translateXBy:rotatedSize.width / 2.0 yBy:rotatedSize.height / 2.0];
                    [transform rotateByDegrees:degrees];
                    switch (imageOrientation)
                    {
                        case 2:
                        case 4:
                        case 5:
                        case 7:
                            [transform scaleXBy:-1.0 yBy:1.0];
                            break;
                        default:
                            break;
                    }
                    NSImage *properOrientation = [[NSImage alloc] initWithSize:rotatedSize];
                    [properOrientation lockFocus];
                    [transform concat];
                    NSRect rect = NSMakeRect(0, 0, thumbnail.size.width, thumbnail.size.height);
                    NSPoint corner = NSMakePoint(-thumbnail.size.width / 2.0, -thumbnail.size.height / 2.0);
                    [thumbnail drawAtPoint:corner fromRect:rect operation:NSCompositingOperationCopy fraction:1.0];
                    [properOrientation unlockFocus];
                    if (properOrientation)
                    {
                        thumbnail = properOrientation;
                    }
                }
            }
        }
        exif_data_unref(exifData);
    }
    return thumbnail;
}
#endif

- (NSDictionary *)metadata
{
    return [self.mutableMetadata copy];
}

- (void)setMetadata:(nullable NSDictionary *)metadata
{
    if ([metadata isKindOfClass:[NSDictionary class]])
    {
        [self.mutableMetadata setDictionary:metadata];
    }
    else
    {
        [self.mutableMetadata removeAllObjects];
    }
    if (self.mutableMetadata.count == 0)
    {
        ExifData *exifData = jpeg_data_get_exif_data(self.jpegData);
        if (exifData)
        {
            jpeg_data_set_exif_data(self.jpegData, NULL);
            exif_data_unref(exifData);
        }
        IptcData *iptcData = jpeg_data_get_iptc_data(self.jpegData);
        if (iptcData)
        {
            jpeg_data_set_iptc_data(self.jpegData, NULL);
            iptc_data_unref(iptcData);
        }
        XmpData *xmpData = jpeg_data_get_xmp_data(self.jpegData);
        if (xmpData)
        {
            jpeg_data_set_xmp_data(self.jpegData, NULL);
            xmp_data_unref(xmpData);
        }
        _xmpSidecar = nil;
    }
}

- (ExifData *)getExifData
{
    ExifData *data = jpeg_data_get_exif_data(self.jpegData);
    if (!data)
    {
        data = exif_data_new();
    }
    return data;
}

- (IptcData *)getIptcData
{
    IptcData *data = jpeg_data_get_iptc_data(self.jpegData);
    if (!data)
    {
        data = iptc_data_new();
    }
    return data;
}

- (SNXmpSidecar *)xmpSidecar
{
    if (_xmpSidecar == nil)
    {
        XmpData *data = jpeg_data_get_xmp_data(self.jpegData);
        _xmpSidecar = [[SNXmpSidecar alloc] initWithXmpData:data orientation:[[self.mutableMetadata objectForKey:(id)kCGImagePropertyOrientation] integerValue]];
        if (data)
        {
            xmp_data_unref(data);
        }
    }
    return _xmpSidecar;
}

#pragma mark - Public methods

- (void)mergeMetadata:(nonnull NSDictionary *)metadata
{
    if (([metadata isKindOfClass:[NSDictionary class]] && metadata.count > 0) == NO) return;
    
    [self.mutableMetadata deeplyAddEntriesFromDictionary:metadata removeIfNullClass:YES];
}

- (BOOL)saveChanges
{
    if (self.fileURL && [self saveToFile:self.fileURL])
    {
        return YES;
    }
    return NO;
}

- (BOOL)saveToFile:(nonnull NSURL *)fileURL
{
    if ([fileURL isKindOfClass:[NSURL class]] == NO) return NO;
    
    BOOL anyChanges = ([self.originalMetadata isEqualToDictionary:self.mutableMetadata] == NO || self.xmpSidecar.cameraRawSettingsChanged);
    
    if (anyChanges == NO && [fileURL isEqual:self.fileURL]) return YES;
    
    if (anyChanges)
    {
        ExifData *exifData = nil;
        IptcData *iptcData = nil;

        // What's new?
        for (NSString *rootKey in self.mutableMetadata)
        {
            id currentRootValue = [self.mutableMetadata objectForKey:rootKey];
            id originalRootValue = [self.originalMetadata objectForKey:rootKey];
            if ([originalRootValue isEqual:currentRootValue] == NO)
            {
                if ([currentRootValue isKindOfClass:[NSDictionary class]])
                {
                    for (NSString *key in (NSDictionary *)currentRootValue)
                    {
                        id currentValue = [(NSDictionary *)currentRootValue objectForKey:key];
                        id originalValue = [(NSDictionary *)originalRootValue objectForKey:key];
                        if (originalValue == nil)
                        {
                            if ([currentValue isKindOfClass:[NSString class]]) originalValue = @"";
                            else if ([currentValue isKindOfClass:[NSNumber class]]) originalValue = @0;
                            else if ([currentValue isKindOfClass:[NSArray class]]) originalValue = @[];
                            else if ([currentValue isKindOfClass:[NSDictionary class]]) originalValue = @{};
                        }
                        if ([originalValue isEqual:currentValue] == NO)
                        {
                            DLog(@"'%@'->'%@' changed. ('%@' to '%@')", rootKey, key, originalValue, currentValue);
                            if ([rootKey isEqual:(id)kCGImagePropertyTIFFDictionary] ||
                                [rootKey isEqual:(id)kCGImagePropertyExifDictionary] ||
                                [rootKey isEqual:(id)kCGImagePropertyGPSDictionary])
                            {
                                exifData = (exifData) ? exifData : [self getExifData];
                                if ([rootKey isEqual:(id)kCGImagePropertyTIFFDictionary])
                                {
                                    [self updateExifData:exifData withValue:currentValue forKey:key inIfd:EXIF_IFD_0];
                                }
                                else if ([rootKey isEqual:(id)kCGImagePropertyGPSDictionary])
                                {
                                    [self updateExifData:exifData withValue:currentValue forKey:key inIfd:EXIF_IFD_GPS];
                                }
                                else
                                {
                                    [self updateExifData:exifData withValue:currentValue forKey:key inIfd:EXIF_IFD_EXIF];
                                }
                            }
                            else if ([rootKey isEqual:(id)kCGImagePropertyIPTCDictionary])
                            {
                                if ([key isEqual:(id)kCGImagePropertyIPTCStarRating] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCRightsUsageTerms] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCCreatorContactInfo] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCScene])
                                {
                                    [self updateIptcCoreData:self.xmpSidecar.xmpData withValue:currentValue forKey:key];
                                }
                                else
                                {
                                    iptcData = (iptcData) ? iptcData : [self getIptcData];
                                    [self updateIptcData:iptcData withValue:currentValue forKey:key];
                                }
                            }
                            else
                            {
                                NSLog(@"'%@' dictionary is not yet supported", rootKey);
                            }
                        }
                    }
                }
                else
                {
                    DLog(@"Root value '%@' changed. ('%@' to '%@')", rootKey, originalRootValue, currentRootValue);
                    exifData = (exifData) ? exifData : [self getExifData];
                    [self updateExifData:exifData withValue:currentRootValue forKey:rootKey inIfd:EXIF_IFD_0];
                }
            }
        }
        
        // What's gone?
        for (id rootKey in self.originalMetadata)
        {
            id currentRootValue = [self.mutableMetadata objectForKey:rootKey];
            id originalRootValue = [self.originalMetadata objectForKey:rootKey];
            if ([originalRootValue isEqual:currentRootValue] == NO)
            {
                if ([currentRootValue isKindOfClass:[NSDictionary class]] && [originalRootValue isKindOfClass:[NSDictionary class]])
                {
                    for (NSString *key in (NSDictionary *)originalRootValue)
                    {
                        if ([(NSDictionary *)currentRootValue objectForKey:key] == nil)
                        {
                            DLog(@"'%@'->'%@' removed", rootKey, key);
                            if ([rootKey isEqual:(id)kCGImagePropertyTIFFDictionary] ||
                                [rootKey isEqual:(id)kCGImagePropertyExifDictionary] ||
                                [rootKey isEqual:(id)kCGImagePropertyGPSDictionary])
                            {
                                exifData = (exifData) ? exifData : [self getExifData];
                                if ([rootKey isEqual:(id)kCGImagePropertyTIFFDictionary])
                                {
                                    [self updateExifData:exifData removingKey:key fromIfd:EXIF_IFD_0];
                                }
                                else if ([rootKey isEqual:(id)kCGImagePropertyGPSDictionary])
                                {
                                    [self updateExifData:exifData removingKey:key fromIfd:EXIF_IFD_GPS];
                                }
                                else
                                {
                                    [self updateExifData:exifData removingKey:key fromIfd:EXIF_IFD_EXIF];
                                }
                            }
                            else if ([rootKey isEqual:(id)kCGImagePropertyIPTCDictionary])
                            {
                                if ([key isEqual:(id)kCGImagePropertyIPTCStarRating] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCRightsUsageTerms] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCCreatorContactInfo] ||
                                    [key isEqual:(id)kCGImagePropertyIPTCScene])
                                {
                                    // IPTC Core
                                    [self updateIptcCoreData:self.xmpSidecar.xmpData removingKey:key];
                                }
                                else
                                {
                                    iptcData = (iptcData) ? iptcData : [self getIptcData];
                                    [self updateIptcData:iptcData removingKey:key];
                                }
                            }
                            else
                            {
                                NSLog(@"'%@' dictionary is not yet supported", rootKey);
                            }
                        }
                    }
                }
                else if (currentRootValue == nil)
                {
                    DLog(@"Root value '%@' removed", rootKey);
                    if ([rootKey isEqual:(id)kCGImagePropertyTIFFDictionary])
                    {
                        exifData = (exifData) ? exifData : [self getExifData];
                        [self updateExifData:exifData removeAllEntriesInIfd:EXIF_IFD_0];
                    }
                    else if ([rootKey isEqual:(id)kCGImagePropertyExifDictionary])
                    {
                        exifData = (exifData) ? exifData : [self getExifData];
                        [self updateExifData:exifData removeAllEntriesInIfd:EXIF_IFD_EXIF];
                    }
                    else if ([rootKey isEqual:(id)kCGImagePropertyGPSDictionary])
                    {
                        exifData = (exifData) ? exifData : [self getExifData];
                        [self updateExifData:exifData removeAllEntriesInIfd:EXIF_IFD_GPS];
                    }
                    else if ([rootKey isEqual:(id)kCGImagePropertyIPTCDictionary])
                    {
                        iptcData = (iptcData) ? iptcData : [self getIptcData];
                        if (iptcData)
                        {
                            jpeg_data_set_iptc_data(self.jpegData, NULL);
                            iptc_data_unref(iptcData);
                            iptcData = NULL;
                        }
                        if (_xmpSidecar)
                        {
                            jpeg_data_set_xmp_data(self.jpegData, NULL);
                            _xmpSidecar = nil;
                        }
                        else
                        {
                            XmpData *xmpData;
                            if ((xmpData = jpeg_data_get_xmp_data(self.jpegData)))
                            {
                                jpeg_data_set_xmp_data(self.jpegData, NULL);
                                xmp_data_unref(xmpData);
                            }
                        }
                    }
                    else
                    {
                        exifData = (exifData) ? exifData : [self getExifData];
                        [self updateExifData:exifData removingKey:rootKey fromIfd:EXIF_IFD_0];
                    }
                }
            }
        }
        
        if (self.xmpSidecar.cameraRawSettingsChanged)
        {
            [self.xmpSidecar commitChanges];
        }
        
        if (exifData)
        {
            jpeg_data_set_exif_data(self.jpegData, exifData);
            exif_data_unref(exifData);
            exifData = NULL;
        }
        if (iptcData)
        {
            iptc_data_sort(iptcData);
            jpeg_data_set_iptc_data(self.jpegData, iptcData);
            iptc_data_unref(iptcData);
            iptcData = NULL;
        }
        if (self.mutableMetadata.count > 0)
        {
            [self.xmpSidecar addMetadataProperties:self.mutableMetadata];
            jpeg_data_set_xmp_data(self.jpegData, self.xmpSidecar.xmpData);
        }
    }
    
    BOOL sucess = NO;
    
    NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSProcessInfo processInfo] globallyUniqueString]];
    if (jpeg_data_save_file(self.jpegData, tempFilePath.UTF8String))
    {
        if ([fileURL isEqual:self.fileURL])
        {
            jpeg_data_unref(self.jpegData);
            self.jpegData = nil;
        }
        NSError *error = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileURL.path])
        {
            if ([[NSFileManager defaultManager] removeItemAtPath:fileURL.path error:&error] == NO)
            {
                NSLog(@"Error removing existing file. (%@)", error);
            }
        }
        if (!error)
        {
            sucess = [[NSFileManager defaultManager] moveItemAtPath:tempFilePath toPath:fileURL.path error:&error];
            if (error)
            {
                NSLog(@"Error moving file into place. (%@)", error);
            }
        }
    }
    
    if (sucess)
    {
        if (self.jpegData == nil)
        {
            self.jpegData = jpeg_data_new_from_file(fileURL.path.UTF8String);;
        }
        [self parseMetadata];
    }
    
    return sucess;
}

+ (BOOL)copyMetadataFromFile:(nonnull NSURL *)sourceURL toFile:(nonnull NSURL *)destinationURL
{
    if ([destinationURL isKindOfClass:[NSURL class]] == NO) return NO;
    if ([sourceURL isKindOfClass:[NSURL class]] == NO) return NO;
    
    __block JPEGData *sourceFile;
    dispatch_sync([SNXmpSidecar libXMLQueue], ^{
        sourceFile = jpeg_data_new_from_file(sourceURL.path.UTF8String);
    });
    if (!sourceFile) return NO;
    
    __block JPEGData *destinationFile;
    dispatch_sync([SNXmpSidecar libXMLQueue], ^{
        destinationFile = jpeg_data_new_from_file(destinationURL.path.UTF8String);
    });
    if (!destinationFile) return NO;

    ExifData *exifData = jpeg_data_get_exif_data(sourceFile);
    ExifData *originalExifData = jpeg_data_get_exif_data(destinationFile);
    if (exifData)
    {
        ExifContent *content;
        ExifEntry *entry;

        content = exifData->ifd[EXIF_IFD_0];
        entry = exif_content_get_entry(content, EXIF_TAG_IMAGE_WIDTH);
        if (entry)
            exif_content_remove_entry(content, entry);
        entry = exif_content_get_entry(content, EXIF_TAG_IMAGE_LENGTH);
        if (entry)
            exif_content_remove_entry(content, entry);
        entry = exif_content_get_entry(content, EXIF_TAG_ORIENTATION);
        if (entry)
            exif_content_remove_entry(content, entry);

        exif_data_trash_thumbnail(exifData); // Also removes IFD1 entries if any

        content = exifData->ifd[EXIF_IFD_EXIF];
        entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_Y_DIMENSION);
        if (entry)
            exif_content_remove_entry(content, entry);
        entry = exif_content_get_entry(content, EXIF_TAG_PIXEL_X_DIMENSION);
        if (entry)
            exif_content_remove_entry(content, entry);
        
        if (originalExifData)
        {
            entry = exif_content_get_entry(originalExifData->ifd[EXIF_IFD_0], EXIF_TAG_ORIENTATION);
            if (entry)
            {
                ExifShort originalOrientation = exif_get_short(entry->data, exif_data_get_byte_order(originalExifData));
                ExifEntry *newEntry = create_tag(exifData, EXIF_IFD_0, EXIF_TAG_ORIENTATION, 2);
                newEntry->format = EXIF_FORMAT_SHORT;
                newEntry->components = 1;
                exif_set_short(newEntry->data, exif_data_get_byte_order(exifData), originalOrientation);
                exif_entry_fix(newEntry);
            }
        }

        jpeg_data_set_exif_data(destinationFile, exifData);
    }
    else if (originalExifData)
    {
        jpeg_data_set_exif_data(destinationFile, NULL);
    }
    IptcData *iptcData = jpeg_data_get_iptc_data(sourceFile);
    IptcData *originalIptcData = jpeg_data_get_iptc_data(destinationFile);
    if (iptcData)
    {
        jpeg_data_set_iptc_data(destinationFile, iptcData);
    }
    else if (originalIptcData)
    {
        jpeg_data_set_iptc_data(destinationFile, NULL);
    }
    XmpData *xmpData = jpeg_data_get_xmp_data(sourceFile);
    XmpData *originalXmpData = jpeg_data_get_xmp_data(destinationFile);
    if (xmpData)
    {
        [SNXmpSidecar removeNameSpaceWithHref:@"http://ns.adobe.com/camera-raw-settings/1.0/" prefix:@"crs" fromData:xmpData];
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:NO createIfAbsent:nil];
        if (node)
        {
            xmlAttrPtr prop = xmlHasProp(node, (xmlChar *)"Orientation");
            if (prop)
            {
                xmlRemoveProp(prop);
            }
        }
        jpeg_data_set_xmp_data(destinationFile, xmpData);
    }
    else if (originalXmpData)
    {
        jpeg_data_set_xmp_data(destinationFile, NULL);
    }

    if (exifData) exif_data_unref(exifData);
    if (originalExifData) exif_data_unref(originalExifData);
    if (iptcData) iptc_data_unref(iptcData);
    if (originalIptcData) iptc_data_unref(originalIptcData);
    if (xmpData) xmp_data_unref(xmpData);
    if (originalXmpData) xmp_data_unref(originalXmpData);

    BOOL result = NO;

    NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSProcessInfo processInfo] globallyUniqueString]];
    if (jpeg_data_save_file(destinationFile, tempFilePath.UTF8String))
    {
        [[NSFileManager defaultManager] removeItemAtPath:destinationURL.path error:nil];
        result = [[NSFileManager defaultManager] moveItemAtPath:tempFilePath toPath:destinationURL.path error:nil];
    }

    jpeg_data_unref(sourceFile);
    jpeg_data_unref(destinationFile);

    return result;
}

#pragma mark - Parsing

- (void)parseMetadata
{
    self.mutableMetadata = [NSMutableDictionary dictionary];
    _xmpSidecar = nil;
    NSInteger exifOrientation = 1;

    unsigned int ifd, i;
    char buf[4096];
    xmlChar *nodeContent;
    NSString *utf8string;

    ExifData *exifData = jpeg_data_get_exif_data(self.jpegData);
    if (exifData)
    {
        ExifContent *content;
        ExifEntry *entry;
        NSMutableDictionary *mutableMetadata;
        for (ifd = 0; ifd < EXIF_IFD_COUNT; ifd++)
        {
            if (ifd != EXIF_IFD_1) // No interest in the thumbnail
            {
                mutableMetadata = self.mutableMetadata;
                content = exifData->ifd[ifd];
                VLog(@"Content %p: ifd=%d\n", content, exif_content_get_ifd(content));
                switch (exif_content_get_ifd(content))
                {
                    case EXIF_IFD_0:
                    {
                        if ([mutableMetadata objectForKey:(id)kCGImagePropertyTIFFDictionary] == nil)
                        {
                            [mutableMetadata setObject:[NSMutableDictionary dictionary] forKey:(id)kCGImagePropertyTIFFDictionary];
                        }
                        mutableMetadata = [mutableMetadata objectForKey:(id)kCGImagePropertyTIFFDictionary];
                        break;
                    }
                    case EXIF_IFD_EXIF:
                    {
                        if ([mutableMetadata objectForKey:(id)kCGImagePropertyExifDictionary] == nil)
                        {
                            [mutableMetadata setObject:[NSMutableDictionary dictionary] forKey:(id)kCGImagePropertyExifDictionary];
                        }
                        mutableMetadata = [mutableMetadata objectForKey:(id)kCGImagePropertyExifDictionary];
                        break;
                    }
                    case EXIF_IFD_GPS:
                    {
                        if ([mutableMetadata objectForKey:(id)kCGImagePropertyGPSDictionary] == nil)
                        {
                            [mutableMetadata setObject:[NSMutableDictionary dictionary] forKey:(id)kCGImagePropertyGPSDictionary];
                        }
                        mutableMetadata = [mutableMetadata objectForKey:(id)kCGImagePropertyGPSDictionary];
                        break;
                    }
                    default:
                        break;
                }
                for (i = 0; i < content->count; i++)
                {
                    entry = content->entries[i];
                    VLog(@"  Entry %p: %s (%s) 0x%04x\t\tSize, Comps: %d, %d\t\tValue: %s",
                         entry,
                         exif_tag_get_name_in_ifd(entry->tag, ifd),
                         exif_format_get_name(entry->format),
                         entry->tag,
                         entry->size,
                         (int)(entry->components),
                         exif_entry_get_value(entry, buf, sizeof(buf)));
                    const char *tagName = exif_tag_get_name_in_ifd(entry->tag, ifd);
                    NSString *key = (tagName) ? [NSString stringWithFormat:@"%s", tagName] : [NSString stringWithFormat:@"0x%04x", entry->tag];
                    BOOL isGPSifd = (entry->parent == entry->parent->parent->ifd[EXIF_IFD_GPS]);
                    
                    if (isGPSifd)
                    {
                        switch ((int)entry->tag)
                        {
                            case EXIF_TAG_GPS_VERSION_ID:
                                key = (id)kCGImagePropertyGPSVersion;
                                break;
                            case EXIF_TAG_GPS_LATITUDE_REF:
                                key = (id)kCGImagePropertyGPSLatitudeRef;
                                break;
                            case EXIF_TAG_GPS_LATITUDE:
                                key = (id)kCGImagePropertyGPSLatitude;
                                break;
                            default:
                                break;
                        }
                    }
                    
                    switch (entry->format)
                    {
                        case EXIF_FORMAT_BYTE:
                        {
                            ExifByte value;
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = entry->data[idx * sizeof(ExifByte)];
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = entry->data[0];
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_SBYTE:
                        {
                            ExifSByte value;
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = entry->data[idx * sizeof(ExifSByte)];
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = entry->data[0];
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_SHORT:
                        {
                            ExifShort value;
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = exif_get_short(entry->data + (idx * sizeof(ExifShort)), exifByteOrder);
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = exif_get_short(entry->data, exifByteOrder);
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_SSHORT:
                        {
                            ExifSShort value;
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = exif_get_sshort(entry->data + (idx * sizeof(ExifSShort)), exifByteOrder);
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = exif_get_sshort(entry->data, exifByteOrder);
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_LONG:
                        {
                            ExifLong value;
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = exif_get_long(entry->data + (idx * sizeof(ExifLong)), exifByteOrder);
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = exif_get_long(entry->data, exifByteOrder);
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_SLONG:
                        {
                            ExifSLong value;
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    value = exif_get_slong(entry->data + (idx * sizeof(ExifSLong)), exifByteOrder);
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                value = exif_get_slong(entry->data, exifByteOrder);
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_RATIONAL:
                        {
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    ExifRational v_rat = exif_get_rational(entry->data + (idx * sizeof(ExifRational)), exifByteOrder);
                                    double value = (!v_rat.denominator) ? v_rat.numerator : (double) v_rat.numerator / (double) v_rat.denominator;
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                                if (isGPSifd && array.count == 3)
                                {
                                    switch ((int)entry->tag)
                                    {
                                        case EXIF_TAG_GPS_LATITUDE:
                                        case EXIF_TAG_GPS_LONGITUDE:
                                        case EXIF_TAG_GPS_DEST_LATITUDE_REF:
                                        case EXIF_TAG_GPS_DEST_LONGITUDE_REF:
                                            [mutableMetadata setObject:[NSNumber numberWithDouble:[array[0] doubleValue] + ([array[1] doubleValue] / 60.0) + ([array[2] doubleValue] / 3600.0)] forKey:key];
                                            break;
                                        case EXIF_TAG_GPS_TIME_STAMP:
                                            [mutableMetadata setObject:[NSString stringWithFormat:@"%02i:%02i:%02i", [array[0] intValue], [array[1] intValue], [array[2] intValue]] forKey:key];
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                ExifRational v_rat = exif_get_rational(entry->data, exifByteOrder);
                                double value = (!v_rat.denominator) ? v_rat.numerator : (double) v_rat.numerator / (double) v_rat.denominator;
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        case EXIF_FORMAT_SRATIONAL:
                        {
                            ExifByteOrder exifByteOrder = exif_data_get_byte_order (entry->parent->parent);
                            if (entry->components > 1)
                            {
                                NSMutableArray *array = [NSMutableArray arrayWithCapacity:entry->components];
                                for (NSInteger idx = 0; idx < entry->components; idx++)
                                {
                                    ExifSRational v_rat = exif_get_srational(entry->data + (idx * sizeof(ExifSRational)), exifByteOrder);
                                    double value = (!v_rat.denominator) ? v_rat.numerator : (double) v_rat.numerator / (double) v_rat.denominator;
                                    [array addObject:@(value)];
                                }
                                [mutableMetadata setObject:array forKey:key];
                            }
                            else
                            {
                                ExifSRational v_rat = exif_get_srational(entry->data, exifByteOrder);
                                double value = (!v_rat.denominator) ? v_rat.numerator : (double) v_rat.numerator / (double) v_rat.denominator;
                                [mutableMetadata setObject:@(value) forKey:key];
                            }
                            break;
                        }
                        default:
                        case EXIF_FORMAT_UNDEFINED:
                        case EXIF_FORMAT_ASCII:
                        {
                            const char *value = exif_entry_get_value(entry, buf, sizeof(buf));
                            utf8string = [NSString stringWithUTF8String:value];
                            [mutableMetadata setObject:(utf8string) ? utf8string : [NSString stringWithFormat:@"%s", value] forKey:key];
                            break;
                        }
                        case EXIF_FORMAT_FLOAT:
                        case EXIF_FORMAT_DOUBLE:
                            // Unsupported for now
                            break;
                    }
                }
            }
        }
        exif_data_unref(exifData);
        // Image IO also has the orientation in the root of the metadata dictionary
        NSMutableDictionary *tiffDictionary = [self.mutableMetadata objectForKey:(id)kCGImagePropertyTIFFDictionary];
        NSNumber *orientation = [tiffDictionary objectForKey:(id)kCGImagePropertyOrientation];
        if (orientation != nil)
        {
            [self.mutableMetadata setObject:orientation forKey:(id)kCGImagePropertyOrientation];
        }
        if ([self.mutableMetadata objectForKey:(id)kCGImagePropertyOrientation])
        {
            exifOrientation = [[self.mutableMetadata objectForKey:(id)kCGImagePropertyOrientation] integerValue];
        }
    }
    
    IptcData *iptcData = jpeg_data_get_iptc_data(self.jpegData);
    if (iptcData)
    {
        const char *strVal;
        IptcDataSet *dataSet;
        NSMutableDictionary *iptcDictionary = [NSMutableDictionary dictionary];
        for (i = 0; i < iptcData->count; i++)
        {
            dataSet = iptcData->datasets[i];
            NSString *key = [NSString stringWithFormat:@"%s", iptc_tag_get_name(dataSet->record, dataSet->tag)];
            switch (iptc_dataset_get_format(dataSet))
            {
                case IPTC_FORMAT_BYTE:
                case IPTC_FORMAT_SHORT:
                case IPTC_FORMAT_LONG:
                {
                    [iptcDictionary setObject:@(iptc_dataset_get_value(dataSet)) forKey:key];
                    break;
                }
                case IPTC_FORMAT_NUMERIC_STRING:
                case IPTC_FORMAT_DATE:
                case IPTC_FORMAT_TIME:
                case IPTC_FORMAT_STRING:
                {
                    if (dataSet->info->repeatable)
                    {
                        NSMutableArray *array = [iptcDictionary objectForKey:key];
                        if ([array isKindOfClass:[NSMutableArray class]] == NO)
                        {
                            array = ([array isKindOfClass:[NSArray class]]) ? [NSMutableArray arrayWithArray:array] : [NSMutableArray arrayWithCapacity:10];
                            [iptcDictionary setObject:array forKey:key];
                        }
                        strVal = iptc_dataset_get_as_str(dataSet, buf, sizeof(buf));
                        if (strVal)
                        {
                            utf8string = [NSString stringWithUTF8String:strVal];
                            [array addObject:(utf8string) ? utf8string : [NSString stringWithFormat:@"%s", strVal]];
                        }
                    }
                    else
                    {
                        strVal = iptc_dataset_get_as_str(dataSet, buf, sizeof(buf));
                        if (strVal)
                        {
                            utf8string = [NSString stringWithUTF8String:strVal];
                            [iptcDictionary setObject:(utf8string) ? utf8string : [NSString stringWithFormat:@"%s", strVal] forKey:key];
                        }
                    }
                    break;
                }
                default:
                case IPTC_FORMAT_UNKNOWN:
                case IPTC_FORMAT_BINARY:
                    break;
            }
        }
        if (iptcDictionary.count > 0)
        {
            [self.mutableMetadata setObject:iptcDictionary forKey:(id)kCGImagePropertyIPTCDictionary];
        }
        iptc_data_unref(iptcData);
    }
    
    XmpData *xmpData = jpeg_data_get_xmp_data(self.jpegData);
    if (xmpData)
    {
        _xmpSidecar = [[SNXmpSidecar alloc] initWithXmpData:xmpData orientation:exifOrientation];
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:NO createIfAbsent:nil];
        if (node)
        {
            // Rating
            NSInteger rating = NSNotFound;
            xmlChar *value = xmlGetProp(node, (xmlChar *)"Rating");
            if (value)
            {
                rating = atoi((char *)value);
                xmlFree(value);
            }
            else
            {
                xmlNode *ratingNode = [SNXmpSidecar maybeNodeWithName:@"Rating" inNode:node nameSpace:[SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/" prefix:@"xmp" inNode:node inXmlDoc:xmpData->xml]];
                if (ratingNode != NULL)
                {
                    nodeContent = xmlNodeGetContent(ratingNode);
                    if (nodeContent)
                    {
                        rating = atoi((char *)nodeContent);
                        xmlFree(nodeContent);
                    }
                    else
                    {
                        rating = 0;
                    }
                }
            }
            if (rating != NSNotFound)
            {
                NSMutableDictionary *iptcDict = [self.mutableMetadata objectForKey:(id)kCGImagePropertyIPTCDictionary];
                if (!iptcDict)
                {
                    iptcDict = [NSMutableDictionary dictionaryWithCapacity:1];
                    [self.mutableMetadata setObject:iptcDict forKey:(id)kCGImagePropertyIPTCDictionary];
                }
                [iptcDict setObject:@(rating) forKey:(id)kCGImagePropertyIPTCStarRating];
            }
        }
        
        node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:YES createIfAbsent:nil];
        if (node)
        {
            // Usage Terms
            xmlNode *usageTermsNode = [SNXmpSidecar maybeNodeWithName:@"li" inNode:[SNXmpSidecar maybeNodeWithName:@"Alt" inNode:[SNXmpSidecar maybeNodeWithName:@"UsageTerms" inNode:node nameSpace:[SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/rights/" prefix:@"xmpRights" inNode:node inXmlDoc:xmpData->xml]] nameSpace:node->ns] nameSpace:node->ns];
            if (usageTermsNode != NULL)
            {
                NSMutableDictionary *iptcDict = [self.mutableMetadata objectForKey:(id)kCGImagePropertyIPTCDictionary];
                if (!iptcDict)
                {
                    iptcDict = [NSMutableDictionary dictionaryWithCapacity:1];
                    [self.mutableMetadata setObject:iptcDict forKey:(id)kCGImagePropertyIPTCDictionary];
                }
                nodeContent = xmlNodeGetContent(usageTermsNode);
                if (nodeContent)
                {
                    [iptcDict setObject:[NSString stringWithUTF8String:(char *)nodeContent] forKey:(id)kCGImagePropertyIPTCRightsUsageTerms];
                    xmlFree(nodeContent);
                }
            }
            
            xmlNsPtr iptc4xmpCoreNS = [SNXmpSidecar maybeNameSpaceWithHref:@"http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/" prefix:@"Iptc4xmpCore" inNode:node inXmlDoc:xmpData->xml];
            
            // Contact Info
            xmlNode *contactInfoNode = [SNXmpSidecar maybeNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:iptc4xmpCoreNS];
            if (contactInfoNode != NULL)
            {
                xmlChar *propStr;
                NSMutableDictionary *props = [NSMutableDictionary dictionaryWithCapacity:10];
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiTelWork")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoPhones];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiAdrCtry")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoCountry];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiUrlWork")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoWebURLs];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiAdrExtadr")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoAddress];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiAdrPcode")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoPostalCode];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiAdrCity")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoCity];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiAdrRegion")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoStateProvince];
                    xmlFree(propStr);
                }
                if ((propStr = xmlGetProp(contactInfoNode, (xmlChar *)"CiEmailWork")))
                {
                    [props setObject:[NSString stringWithUTF8String:(char *)propStr] forKey:(id)kCGImagePropertyIPTCContactInfoEmails];
                    xmlFree(propStr);
                }
                if (props.count > 0)
                {
                    NSMutableDictionary *iptcDict = [self.mutableMetadata objectForKey:(id)kCGImagePropertyIPTCDictionary];
                    if (!iptcDict)
                    {
                        iptcDict = [NSMutableDictionary dictionaryWithCapacity:1];
                        [self.mutableMetadata setObject:iptcDict forKey:(id)kCGImagePropertyIPTCDictionary];
                    }
                    [iptcDict setObject:props forKey:(id)kCGImagePropertyIPTCCreatorContactInfo];
                }
            }
            // Scene
            xmlNode *sceneBagNode = [SNXmpSidecar maybeNodeWithName:@"Bag" inNode:[SNXmpSidecar maybeNodeWithName:@"Scene" inNode:node nameSpace:iptc4xmpCoreNS] nameSpace:node->ns];
            if (sceneBagNode != NULL)
            {
                NSMutableArray *scenes = [NSMutableArray arrayWithCapacity:100];
                xmlNode *liNode = sceneBagNode->children;
                while (liNode != NULL)
                {
                    if (strcmp((char *)liNode->name, "li") == 0)
                    {
                        nodeContent = xmlNodeGetContent(liNode);
                        if (nodeContent)
                        {
                            [scenes addObject:[NSString stringWithUTF8String:(char *)nodeContent]];
                            xmlFree(nodeContent);
                        }
                    }
                    liNode = liNode->next;
                }
                if (scenes.count > 0)
                {
                    NSMutableDictionary *iptcDict = [self.mutableMetadata objectForKey:(id)kCGImagePropertyIPTCDictionary];
                    if (!iptcDict)
                    {
                        iptcDict = [NSMutableDictionary dictionaryWithCapacity:1];
                        [self.mutableMetadata setObject:iptcDict forKey:(id)kCGImagePropertyIPTCDictionary];
                    }
                    [iptcDict setObject:scenes forKey:(id)kCGImagePropertyIPTCScene];
                }
            }
        }
        xmp_data_unref(xmpData);
    }
    
    self.originalMetadata = CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFPropertyListRef)self.mutableMetadata, kCFPropertyListImmutable));
}

#pragma mark - Exif

- (void)updateExifData:(ExifData *)exifData withValue:(id)newValue forKey:(NSString *)key inIfd:(ExifIfd)ifd
{
    if (!exifData) return;
    if (!newValue) return;
    if (!key) return;
    
    unsigned char format_size;
    unsigned long components;
    ExifTag tag = exif_tag_from_name([key UTF8String]);
    if (tag || (ifd == EXIF_IFD_GPS && [key isEqualToString:(id)kCGImagePropertyGPSVersion])) // GPSVersion is tag number 0
    {
        ExifByteOrder byteOrder = exif_data_get_byte_order(exifData);
        ExifContent *content = exifData->ifd[ifd];
        ExifEntry *entry = exif_content_get_entry(content, tag);
        ExifFormat format = EXIF_FORMAT_UNDEFINED;
        if (entry)
        {
            format = entry->format;
            exif_content_remove_entry(content, entry);
        }
        if (format == EXIF_FORMAT_UNDEFINED)
        {
            format = format_for_tag(tag, ifd);
        }
        switch (format)
        {
            default:
            case EXIF_FORMAT_UNDEFINED:
            {
                const char *charVal;
                ExifLong length;
                if ([newValue isKindOfClass:[NSString class]])
                {
                    charVal = [(NSString *)newValue UTF8String];
                    length = (ExifLong)strlen(charVal);
                }
                else if ([newValue isKindOfClass:[NSData class]])
                {
                    length = (ExifLong)[(NSData *)newValue length];
                    [(NSData *)newValue getBytes:&charVal length:length];
                }
                else
                {
                    charVal = [[newValue description] UTF8String];
                    length = (ExifLong)strlen(charVal);
                }
                entry = create_tag(exifData, ifd, tag, length);
                memcpy(entry->data, charVal, length);
                break;
            }
            case EXIF_FORMAT_ASCII:
            {
                newValue = ([newValue isKindOfClass:[NSString class]]) ? newValue : [newValue description];
                if ([(NSString *)newValue length] == 0) return;
                const char *strVal = [(NSString *)newValue UTF8String];
                entry = create_tag(exifData, ifd, tag, (ExifLong)strlen(strVal) + 1);
                memcpy(entry->data, strVal, strlen(strVal));
                break;
            }
            case EXIF_FORMAT_BYTE:
            {
                char value;
                format_size = exif_format_get_size(EXIF_FORMAT_BYTE);
                if ([newValue isKindOfClass:[NSString class]])
                {
                    NSArray *comps = [(NSString *)newValue componentsSeparatedByString:@"."];
                    if (comps.count == 4)
                    {
                        newValue = comps;
                    }
                }
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        value = ([(NSArray *)newValue[idx] isKindOfClass:[NSNumber class]]) ? [(NSNumber *)(NSArray *)newValue[idx] unsignedCharValue] : [[NSNumber numberWithInteger:[[(NSArray *)newValue[idx] description] integerValue]] unsignedCharValue];
                        memcpy(entry->data + (idx * format_size), &value, sizeof(value));
                    }
                }
                else
                {
                    value = ([newValue isKindOfClass:[NSString class]]) ? [newValue intValue] : [newValue unsignedCharValue];
                    memcpy(entry->data, &value, sizeof(value));
                }
                break;
            }
            case EXIF_FORMAT_SBYTE:
            {
                char value;
                format_size = exif_format_get_size(EXIF_FORMAT_SBYTE);
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        value = [[(NSArray *)newValue objectAtIndex:idx] charValue];
                        memcpy(entry->data + (idx * format_size), &value, sizeof(value));
                    }
                }
                else
                {
                    value = [newValue charValue];
                    memcpy(entry->data, &value, sizeof(value));
                }
                break;
            }
            case EXIF_FORMAT_SHORT:
            {
                format_size = exif_format_get_size(EXIF_FORMAT_SHORT);
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        exif_set_short(entry->data + (idx * format_size), byteOrder, [[(NSArray *)newValue objectAtIndex:idx] unsignedShortValue]);
                    }
                }
                else
                {
                    exif_set_short(entry->data, byteOrder, [newValue unsignedShortValue]);
                }
                break;
            }
            case EXIF_FORMAT_SSHORT:
            {
                format_size = exif_format_get_size(EXIF_FORMAT_SSHORT);
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        exif_set_sshort(entry->data + (idx * format_size), byteOrder, [[(NSArray *)newValue objectAtIndex:idx] shortValue]);
                    }
                }
                else
                {
                    exif_set_sshort(entry->data, byteOrder, [newValue shortValue]);
                }
                break;
            }
            case EXIF_FORMAT_LONG:
            {
                format_size = exif_format_get_size(EXIF_FORMAT_LONG);
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        exif_set_long(entry->data + (idx * format_size), byteOrder, [[(NSArray *)newValue objectAtIndex:idx] unsignedIntValue]);
                    }
                }
                else
                {
                    exif_set_long(entry->data, byteOrder, [newValue unsignedIntValue]);
                }
                break;
            }
            case EXIF_FORMAT_SLONG:
            {
                format_size = exif_format_get_size(EXIF_FORMAT_SLONG);
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        exif_set_slong(entry->data + (idx * format_size), byteOrder, [[(NSArray *)newValue objectAtIndex:idx] intValue]);
                    }
                }
                else
                {
                    exif_set_slong(entry->data, byteOrder, [newValue intValue]);
                }
                break;
            }
            case EXIF_FORMAT_RATIONAL:
            {
                ExifRational v_rat;
                format_size = exif_format_get_size(EXIF_FORMAT_RATIONAL);
                if (ifd == EXIF_IFD_GPS &&
                    (tag == EXIF_TAG_GPS_LATITUDE || tag == EXIF_TAG_GPS_LONGITUDE ||
                     tag == EXIF_TAG_GPS_DEST_LATITUDE || tag == EXIF_TAG_GPS_DEST_LONGITUDE))
                {
                    // We don't *have* to be specific here, but if we don't, we'd lose precision in
                    // the exif_entry_fix() when the denominator has been capped at 10000000.
                    entry = create_tag(exifData, ifd, tag, (ExifLong)(3 * format_size));
                    entry->components = 3;
                    
                    double val = fabs([(NSNumber *)newValue doubleValue]);
                    double deg = (int)val;
                    v_rat.denominator = 1;
                    v_rat.numerator = deg;
                    exif_set_rational(entry->data, byteOrder, v_rat);
                    
                    val = val - deg;
                    val = val * 60;
                    double minutes = (int) val;
                    v_rat.denominator = 1;
                    v_rat.numerator = minutes;
                    exif_set_rational(entry->data + format_size, byteOrder, v_rat);
                    
                    val = val - minutes;
                    val = val * 60;
                    double seconds = val;
                    v_rat.denominator = 100;
                    v_rat.numerator = seconds * v_rat.denominator;
                    exif_set_rational(entry->data + (2 * format_size), byteOrder, v_rat);
                }
                else
                {
                    if ([newValue isKindOfClass:[NSString class]])
                    {
                        NSArray *comps = [(NSString *)newValue componentsSeparatedByString:@":"];
                        if (comps.count == 3)
                        {
                            newValue = comps;
                        }
                    }
                    components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                    entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                    entry->components = components;
                    v_rat.denominator = 10000000;
                    if ([newValue isKindOfClass:[NSArray class]])
                    {
                        for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                        {
                            v_rat.numerator = fabs([[(NSArray *)newValue objectAtIndex:idx] doubleValue] * v_rat.denominator);
                            exif_set_rational(entry->data + (idx * format_size), byteOrder, v_rat);
                        }
                    }
                    else
                    {
                        v_rat.numerator = fabs([(NSNumber *)newValue doubleValue] * v_rat.denominator);
                        exif_set_rational(entry->data, byteOrder, v_rat);
                    }
                }
                break;
            }
            case EXIF_FORMAT_SRATIONAL:
            {
                ExifSRational v_rat;
                format_size = exif_format_get_size(EXIF_FORMAT_SRATIONAL);
                if ([newValue isKindOfClass:[NSString class]])
                {
                    NSArray *comps = [(NSString *)newValue componentsSeparatedByString:@":"];
                    if (comps.count == 3)
                    {
                        newValue = comps;
                    }
                }
                components = ([newValue isKindOfClass:[NSArray class]]) ? [(NSArray *)newValue count] : 1;
                entry = create_tag(exifData, ifd, tag, (ExifLong)(components * format_size));
                entry->components = components;
                v_rat.denominator = 10000000;
                if ([newValue isKindOfClass:[NSArray class]])
                {
                    for (NSInteger idx = 0; idx < [(NSArray *)newValue count]; idx++)
                    {
                        v_rat.numerator = fabs([[(NSArray *)newValue objectAtIndex:idx] doubleValue] * v_rat.denominator);
                        exif_set_srational(entry->data + (idx * format_size), byteOrder, v_rat);
                    }
                }
                else
                {
                    v_rat.numerator = fabs([(NSNumber *)newValue doubleValue] * v_rat.denominator);
                    exif_set_srational(entry->data, byteOrder, v_rat);
                }
                break;
            }
            case EXIF_FORMAT_FLOAT:
            case EXIF_FORMAT_DOUBLE:
                // Unsupported for now
                break;
        }
        if (entry)
        {
            entry->format = format;
            exif_entry_fix(entry);
        }
    }
    else
    {
        NSLog(@"Unsupported Exif tag: 0x%04x", tag);
    }
}

- (void)updateExifData:(ExifData *)exifData removingKey:(NSString *)key fromIfd:(ExifIfd)ifd
{
    if (!exifData) return;
    if (!key) return;
    ExifTag tag = exif_tag_from_name(key.UTF8String);
    if (tag || (ifd == EXIF_IFD_GPS && [key isEqualToString:(id)kCGImagePropertyGPSVersion])) // GPSVersion is tag number 0
    {
        ExifContent *content = exifData->ifd[ifd];
        ExifEntry *entry = exif_content_get_entry(content, tag);
        if (entry)
        {
            exif_content_remove_entry(content, entry);
        }
    }
}

- (void)updateExifData:(ExifData *)exifData removeAllEntriesInIfd:(ExifIfd)ifd
{
    if (!exifData) return;
    ExifContent *content = exifData->ifd[ifd];
    while (content && content->count > 0)
    {
        exif_content_remove_entry(content, content->entries[0]);
    }
}

#pragma mark - IPTC

- (void)updateIptcData:(IptcData *)iptcData withValue:(id)newValue forKey:(NSString *)key
{
    if (!iptcData) return;
    if (!newValue) return;
    if (!key) return;
    
    IptcTag tag;
    IptcRecord record;
    IptcDataSet *ds;
    const char *strVal;
    if (iptc_tag_find_by_name(key.UTF8String, &record, &tag) == 0) // 0 = Found
    {
        const IptcTagInfo *info = iptc_tag_get_info(record, tag);
        
        while ((ds = iptc_data_get_dataset(iptcData, record, tag)))
        {
            iptc_data_remove_dataset(iptcData, ds);
            iptc_dataset_unref(ds);
        }
        
        if ([newValue isKindOfClass:[NSArray class]])
        {
            if ([[(NSArray *)newValue firstObject] isKindOfClass:[NSString class]])
            {
                for (NSString *value in (NSArray *)newValue)
                {
                    strVal = [(NSString *)value UTF8String];
                    ds = iptc_dataset_new();
                    iptc_dataset_set_tag(ds, record, tag);
                    iptc_dataset_set_data(ds, (unsigned char *)strVal, (unsigned int)MIN(strlen(strVal), info->maxbytes), IPTC_DONT_VALIDATE);
                    iptc_data_add_dataset(iptcData, ds);
                    iptc_dataset_unref(ds);
                }
            }
        }
        else if (([newValue isKindOfClass:[NSNull class]] || ([newValue isKindOfClass:[NSString class]] && [(NSString *)newValue length] == 0) == NO))
        {
            ds = iptc_dataset_new();
            iptc_dataset_set_tag(ds, record, tag);
            switch (info->format)
            {
                case IPTC_FORMAT_BYTE:
                    iptc_dataset_set_value(ds, [newValue unsignedCharValue], IPTC_DONT_VALIDATE);
                    break;
                case IPTC_FORMAT_SHORT:
                    iptc_dataset_set_value(ds, [newValue unsignedShortValue], IPTC_DONT_VALIDATE);
                    break;
                case IPTC_FORMAT_LONG:
                    iptc_dataset_set_value(ds, [newValue unsignedIntValue], IPTC_DONT_VALIDATE);
                    break;
                default:
                    newValue = ([newValue isKindOfClass:[NSString class]]) ? newValue : [newValue description];
                    strVal = [(NSString *)newValue UTF8String];
                    iptc_dataset_set_data(ds, (unsigned char *)strVal, (unsigned int)MIN(strlen(strVal), info->maxbytes), IPTC_DONT_VALIDATE);
                    break;
            }
            iptc_data_add_dataset(iptcData, ds);
            iptc_dataset_unref(ds);
        }
    }
    else
    {
        NSLog(@"Unsupported IPTC tag '%@'", key);
    }
}

- (void)updateIptcData:(IptcData *)iptcData removingKey:(NSString *)key
{
    if (!iptcData) return;
    if (!key) return;
    IptcTag tag;
    IptcRecord record;
    IptcDataSet *ds;
    if (iptc_tag_find_by_name(key.UTF8String, &record, &tag) == 0)
    {
        while ((ds = iptc_data_get_dataset(iptcData, record, tag)))
        {
            iptc_data_remove_dataset(iptcData, ds);
            iptc_dataset_unref(ds);
        }
    }
}

#pragma mark - IPTC Core
// https://www.iptc.org/std/Iptc4xmpCore/1.0/specification/Iptc4xmpCore_1.0-spec-XMPSchema_8.pdf

- (void)updateIptcCoreData:(XmpData *)xmpData withValue:(id)newValue forKey:(NSString *)key
{
    if (!xmpData) return;
    if (!newValue) return;
    if (!key) return;

    NSDate *createDate = nil;
    NSString *dateTime = [[self.mutableMetadata objectForKey:(id)kCGImagePropertyTIFFDictionary] objectForKey:(id)kCGImagePropertyTIFFDateTime];
    if ([dateTime isKindOfClass:[NSString class]] && dateTime.length == 19 && [dateTime characterAtIndex:10] == ' ')
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setFormatterBehavior:NSDateFormatterBehavior10_4];
        [df setDateFormat:@"yyyy-MM-dd' 'HH:mm:ss"];
        createDate = [df dateFromString:dateTime];
    }

    if ([key isEqual:(id)kCGImagePropertyIPTCStarRating])
    {
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:NO createIfAbsent:(createDate) ? createDate : [NSDate date]];
        xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/" prefix:@"xmp" inNode:node inXmlDoc:xmpData->xml];
        xmlNode *ratingNode = [SNXmpSidecar maybeNodeWithName:@"Rating" inNode:node nameSpace:ns];
        if (ratingNode != NULL)
        {
            xmlNodeSetContent(ratingNode, (xmlChar *)[[NSString stringWithFormat:@"%lu", (long)[newValue integerValue]] UTF8String]);
        }
        else // The ImageIO way. Is it more correct to add an xmp:Rating child?
        {
            xmlSetProp(node, (xmlChar *)"xmp:Rating", (xmlChar *)[[NSString stringWithFormat:@"%lu", (long)[newValue integerValue]] UTF8String]);
        }
    }
    else
    {
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:YES createIfAbsent:(createDate) ? createDate : [NSDate date]];
        if ([key isEqual:(id)kCGImagePropertyIPTCRightsUsageTerms])
        {
            if ([(NSString *)newValue length] > 0)
            {
                xmlNsPtr ns = [SNXmpSidecar needNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/rights/" prefix:@"xmpRights" inNode:node inXmlDoc:xmpData->xml];
                xmlNode *liNode = [SNXmpSidecar needNodeWithName:@"li" inNode:[SNXmpSidecar needNodeWithName:@"Alt" inNode:[SNXmpSidecar needNodeWithName:@"UsageTerms" inNode:node nameSpace:ns] nameSpace:node->ns] nameSpace:node->ns];
                xmlSetProp(liNode, (xmlChar *)"xml:lang", (xmlChar *)"x-default");
                xmlNodeSetContent(liNode, (xmlChar *)[(NSString *)newValue UTF8String]);
            }
            else
            {
                [self updateIptcCoreData:xmpData removingKey:key];
            }
        }
        else
        {
            xmlNsPtr ns = [SNXmpSidecar needNameSpaceWithHref:@"http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/" prefix:@"Iptc4xmpCore" inNode:node inXmlDoc:xmpData->xml];
            if ([key isEqual:(id)kCGImagePropertyIPTCCreatorContactInfo])
            {
                xmlNode *contactInfoNode = [SNXmpSidecar maybeNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                if (contactInfoNode)
                {
                    xmlUnlinkNode(contactInfoNode);
                }
                contactInfoNode = NULL;
                if ([newValue isKindOfClass:[NSDictionary class]] && [(NSDictionary *)newValue count] > 0)
                {
                    NSString *value;
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoCity])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoCity];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiAdrCity", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoCountry])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoCountry];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiAdrCtry", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoAddress])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoAddress];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiAdrExtadr", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoPostalCode])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoPostalCode];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiAdrPcode", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoStateProvince])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoStateProvince];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiAdrRegion", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoEmails])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoEmails];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiEmailWork", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoPhones])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoPhones];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiTelWork", (xmlChar *)[value UTF8String]);
                        }
                    }
                    if ([(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoWebURLs])
                    {
                        value = [(NSDictionary *)newValue objectForKey:(id)kCGImagePropertyIPTCContactInfoWebURLs];
                        if (value.length > 0)
                        {
                            contactInfoNode = (contactInfoNode) ? contactInfoNode : [SNXmpSidecar needNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                            xmlSetProp(contactInfoNode, (xmlChar *)"Iptc4xmpCore:CiUrlWork", (xmlChar *)[value UTF8String]);
                        }
                    }
                }
            }
            else if ([key isEqual:(id)kCGImagePropertyIPTCScene])
            {
                xmlNode *sceneNode = [SNXmpSidecar maybeNodeWithName:@"Scene" inNode:node nameSpace:ns];
                if (sceneNode)
                {
                    xmlUnlinkNode(sceneNode);
                }
                if ([newValue isKindOfClass:[NSArray class]] && [(NSArray *)newValue count] > 0)
                {
                    xmlNode *sceneBagNode = [SNXmpSidecar needNodeWithName:@"Bag" inNode:[SNXmpSidecar needNodeWithName:@"Scene" inNode:node nameSpace:ns] nameSpace:node->ns];
                    for (NSString *scene in (NSArray *)newValue)
                    {
                        if ([scene isKindOfClass:[NSString class]])
                        {
                            xmlNode *itemNode = xmlNewNode(node->ns, (xmlChar *)"li");
                            xmlNodeSetContent(itemNode, (xmlChar *)[scene UTF8String]);
                            xmlAddChild(sceneBagNode, itemNode);
                        }
                    }
                }
            }
        }
    }
}

- (void)updateIptcCoreData:(XmpData *)xmpData removingKey:(NSString *)key
{
    if (!xmpData) return;
    if (!key) return;
    
    if ([key isEqual:(id)kCGImagePropertyIPTCStarRating])
    {
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:NO createIfAbsent:nil];
        if (!node) return;
        
        xmlAttrPtr prop = xmlHasProp(node, (xmlChar *)"Rating");
        if (prop)
        {
            xmlRemoveProp(prop);
        }
        xmlNode *ratingNode = [SNXmpSidecar maybeNodeWithName:@"Rating" inNode:node nameSpace:[SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/" prefix:@"xmp" inNode:node inXmlDoc:xmpData->xml]];
        if (ratingNode != NULL)
        {
            xmlNodeSetContent(ratingNode, (xmlChar *)"0\0");
        }
    }
    else
    {
        xmlNode *node = [SNXmpSidecar xapDescriptionNodeInData:xmpData preferringXMPCore:YES createIfAbsent:nil];
        if (!node) return;
        
        if ([key isEqual:(id)kCGImagePropertyIPTCRightsUsageTerms])
        {
            xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:@"http://ns.adobe.com/xap/1.0/rights/" prefix:@"xmpRights" inNode:node inXmlDoc:xmpData->xml];
            if (ns)
            {
                xmlNode *unlinkNode = [SNXmpSidecar maybeNodeWithName:@"UsageTerms" inNode:node nameSpace:ns];
                if (unlinkNode)
                {
                    xmlUnlinkNode(unlinkNode);
                }
            }
        }
        else
        {
            xmlNsPtr ns = [SNXmpSidecar maybeNameSpaceWithHref:@"http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/" prefix:@"Iptc4xmpCore" inNode:node inXmlDoc:xmpData->xml];
            if (ns)
            {
                if ([key isEqual:(id)kCGImagePropertyIPTCCreatorContactInfo])
                {
                    xmlNode *unlinkNode = [SNXmpSidecar maybeNodeWithName:@"CreatorContactInfo" inNode:node nameSpace:ns];
                    if (unlinkNode)
                    {
                        xmlUnlinkNode(unlinkNode);
                    }
                }
                else if ([key isEqual:(id)kCGImagePropertyIPTCScene])
                {
                    xmlNode *unlinkNode = [SNXmpSidecar maybeNodeWithName:@"Scene" inNode:node nameSpace:ns];
                    if (unlinkNode)
                    {
                        xmlUnlinkNode(unlinkNode);
                    }
                }
            }
        }
    }
}

@end
