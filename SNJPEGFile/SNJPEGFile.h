//
//  SNJPEGFile.h
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain.
//

#import <Foundation/Foundation.h>
#import <ImageIO/ImageIO.h>
#import "SNXmpSidecar.h"
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

@interface SNJPEGFile : NSObject

@property (nonatomic, readonly, nonnull) NSURL *fileURL;
@property (nonatomic, copy, nullable) NSDictionary *metadata;
@property (nonatomic, readonly, nonnull) SNXmpSidecar *xmpSidecar;
#if TARGET_OS_IPHONE
@property (nonatomic, readonly, nullable) UIImage *thumbnail;
#else
@property (nonatomic, readonly, nullable) NSImage *thumbnail;
#endif

/**
 * Copy metadata from one file to another.
 *
 * @param sourceURL File URL pointing to the source file to copy metadata from.
 * @param destinationURL File URL for the destination file that will have its metadata replaced.
 * @note Image Orientation and Adobe Camera Raw settings will not be copied.
 * @return True if the metadata was copied.
 */
+ (BOOL)copyMetadataFromFile:(nonnull NSURL *)sourceURL toFile:(nonnull NSURL *)destinationURL;

- (nullable instancetype)initWithFileURL:(nonnull NSURL *)fileURL;
- (nullable instancetype)initWithData:(nonnull NSData *)data;

- (void)mergeMetadata:(nonnull NSDictionary<NSString *, id> *)metadata;

- (BOOL)saveChanges;
- (BOOL)saveToFile:(nonnull NSURL *)fileURL;

@end
