/*! \file xmp-mem.h
 *  \brief Define the XmpMem data type and the associated functions.
 *  XmpMem defines the memory management functions used within libxmpdata.
 */
/* xmp-mem.h
 *
 * Copyright (c) 2003 Lutz Mueller <lutz@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA.
 */

#ifndef __XMP_MEM_H__
#define __XMP_MEM_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef uint32_t	XmpLong;          /* 4 bytes */

/*! Should work like calloc()
 *
 *  \param[in] s the size of the block to allocate.
 *  \return the allocated memory and initialized. 
 */
typedef void * (* XmpMemAllocFunc)   (XmpLong s);

/*! Should work like realloc()
 *
 * \param[in] p the pointer to reallocate
 * \param[in] s the size of the reallocated block
 * \return allocated memory 
 */
typedef void * (* XmpMemReallocFunc) (void *p, XmpLong s);

/*! Free method for XmpMem
 *
 * \param[in] p the pointer to free
 */
typedef void   (* XmpMemFreeFunc)    (void *p);

/*! XmpMem define a memory allocator */
typedef struct _XmpMem XmpMem;

/*! Create a new XmpMem
 *
 * \param[in] a the allocator function
 * \param[in] r the reallocator function
 * \param[in] f the free function
 */
XmpMem *xmp_mem_new   (XmpMemAllocFunc a, XmpMemReallocFunc r,
			 XmpMemFreeFunc f);
/*! Refcount an XmpMem
 */
void     xmp_mem_ref   (XmpMem *);

/*! Unrefcount an XmpMem.
 * If the refcount reaches 0, the XmpMem is freed
 */
void     xmp_mem_unref (XmpMem *);

void *xmp_mem_alloc   (XmpMem *m, XmpLong s);
void *xmp_mem_realloc (XmpMem *m, void *p, XmpLong s);
void  xmp_mem_free    (XmpMem *m, void *p);

/*! Create a new XmpMem with default values for your convenience
 *
 * \return return a new default XmpMem
 */
XmpMem *xmp_mem_new_default (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __XMP_MEM_H__ */
