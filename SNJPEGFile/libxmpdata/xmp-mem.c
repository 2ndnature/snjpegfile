#include "xmp-mem.h"

#include <stdlib.h>

struct _XmpMem {
	unsigned int ref_count;
	XmpMemAllocFunc alloc_func;
	XmpMemReallocFunc realloc_func;
	XmpMemFreeFunc free_func;
};

/*! Default memory allocation function. */
static void *
xmp_mem_alloc_func (XmpLong ds)
{
	return calloc ((size_t) ds, 1);
}

/*! Default memory reallocation function. */
static void *
xmp_mem_realloc_func (void *d, XmpLong ds)
{
	return realloc (d, (size_t) ds);
}

/*! Default memory free function. */
static void
xmp_mem_free_func (void *d)
{
	free (d);
}

XmpMem *
xmp_mem_new (XmpMemAllocFunc alloc_func, XmpMemReallocFunc realloc_func,
	      XmpMemFreeFunc free_func)
{
	XmpMem *mem;

	if (!alloc_func && !realloc_func) 
		return NULL;
	mem = alloc_func ? alloc_func (sizeof (XmpMem)) :
		           realloc_func (NULL, sizeof (XmpMem));
	if (!mem) return NULL;
	mem->ref_count = 1;

	mem->alloc_func   = alloc_func;
	mem->realloc_func = realloc_func;
	mem->free_func    = free_func;

	return mem;
}

void
xmp_mem_ref (XmpMem *mem)
{
	if (!mem) return;
	mem->ref_count++;
}

void
xmp_mem_unref (XmpMem *mem)
{
	if (!mem) return;
	if (!--mem->ref_count)
		xmp_mem_free (mem, mem);
}

void
xmp_mem_free (XmpMem *mem, void *d)
{
	if (!mem) return;
	if (mem->free_func) {
		mem->free_func (d);
		return;
	}
}

void *
xmp_mem_alloc (XmpMem *mem, XmpLong ds)
{
	if (!mem) return NULL;
	if (mem->alloc_func || mem->realloc_func)
		return mem->alloc_func ? mem->alloc_func (ds) :
					 mem->realloc_func (NULL, ds);
	return NULL;
}

void *
xmp_mem_realloc (XmpMem *mem, void *d, XmpLong ds)
{
	return (mem && mem->realloc_func) ? mem->realloc_func (d, ds) : NULL;
}

XmpMem *
xmp_mem_new_default (void)
{
	return xmp_mem_new (xmp_mem_alloc_func, xmp_mem_realloc_func,
			     xmp_mem_free_func);
}
