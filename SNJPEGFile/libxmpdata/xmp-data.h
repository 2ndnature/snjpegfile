//
//  xmp-data.h
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain. You are free to do
//  whatever you want with it.

#ifndef xmp_data_h
#define xmp_data_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
    
    /*! Represents the XMP data found in an image */
    typedef struct _XmpData        XmpData;
    typedef struct _XmpDataPrivate XmpDataPrivate;

#include "xmp-mem.h"
#include <libxml/xpath.h>
   
    /*! Represents the XMP data found in an image */
    struct _XmpData
    {
        xmlDocPtr xml;
        
        XmpDataPrivate *priv;
    };
        
    /*! Allocate a new #XmpData.
     *
     * \return allocated #XmpData, or NULL on error
     */
    XmpData *xmp_data_new           (void);
    
    /*! Allocate a new #XmpData using the given memory allocator.
     *
     * \return allocated #XmpData, or NULL on error
     */
    XmpData *xmp_data_new_mem       (XmpMem *);
    
    /*! Allocate a new #XmpData loaded with standard xml contents.
     *
     * \return allocated #XmpData, or NULL on error
     */
    XmpData *xmp_data_new_xml (void);
    
    /*! Allocate a new #XmpData and load XMP data from a memory buffer.
     *
     * \param[in] data pointer to JPEG header with XMP data
     * \param[in] size number of bytes of data at data
     * \return allocated #XmpData, or NULL on error
     */
    XmpData *xmp_data_new_from_data (const unsigned char *data,
                                     unsigned int size);
    
    /*! Load the #XmpData structure from the JPEG header with XMP data in the given
     * memory buffer.
     *
     * \param[in,out] data XMP data
     * \param[in] d pointer to raw JPEG or XMP data
     * \param[in] size number of bytes of data at d
     */
    void      xmp_data_load_data (XmpData *data, const unsigned char *d,
                                  unsigned int size);
    
    /*! Store raw XMP data representing the #XmpData structure into a memory
     * buffer. The buffer is allocated by this function and must subsequently be
     * freed by the caller using the matching free function as used by the #XmpMem
     * in use by this #XmpData.
     *
     * \param[in] data XMP data
     * \param[out] d pointer to buffer pointer containing raw XMP data on return
     * \param[out] ds pointer to variable to hold the number of bytes of
     *   data at d, or set to 0 on error
     */
    void      xmp_data_save_data (XmpData *data, unsigned char **d,
                                  unsigned int *ds);
    
    void      xmp_data_ref   (XmpData *data);
    void      xmp_data_unref (XmpData *data);
    void      xmp_data_free  (XmpData *data);
    
    void      xmp_data_dump  (XmpData *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* xmp_data_h */
