//
//  xmp-data.c
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain. You are free to do
//  whatever you want with it.

#include "xmp-data.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define XMP_IDENTIFIER_STR  "http://ns.adobe.com/xap/1.0/\0"

struct _XmpDataPrivate
{
    XmpMem *mem;
    
    unsigned int ref_count;
};

static void *
xmp_data_alloc (XmpData *data, unsigned int i)
{
    void *d;
    
    if (!data || !i)
        return NULL;
    
    d = xmp_mem_alloc (data->priv->mem, i);
    if (d)
        return d;
    
    return NULL;
}

XmpData *
xmp_data_new (void)
{
    XmpMem *mem = xmp_mem_new_default ();
    XmpData *d = xmp_data_new_mem (mem);
    
    xmp_mem_unref (mem);
    
    return d;
}

XmpData *
xmp_data_new_mem (XmpMem *mem)
{
    XmpData *data;
    
    if (!mem)
        return NULL;
    
    data = xmp_mem_alloc (mem, sizeof (XmpData));
    if (!data)
        return (NULL);
    data->priv = xmp_mem_alloc (mem, sizeof (XmpDataPrivate));
    if (!data->priv) {
        xmp_mem_free (mem, data);
        return (NULL);
    }
    data->priv->ref_count = 1;
    
    data->priv->mem = mem;
    xmp_mem_ref (mem);
    
    return (data);
}

XmpData *
xmp_data_new_xml (void)
{
    XmpData *xmp;
    
    xmp = xmp_data_new ();

    const char *data = "\xFF\xE1\x01\x7Ehttp://ns.adobe.com/xap/1.0/\0<?xpacket begin=\"\xEF\xBB\xBF\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?> <x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"XMP Core 5.4.0\"> <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"> <rdf:Description rdf:about=\"\" xmlns:xmp=\"http://ns.adobe.com/xap/1.0/\" xmlns:photoshop=\"http://ns.adobe.com/photoshop/1.0/\"/> </rdf:RDF> </x:xmpmeta> <?xpacket end=\"w\"?>\0";

    xmp_data_load_data (xmp, (unsigned char *)data, 385);
    return (xmp);
}

XmpData *
xmp_data_new_from_data (const unsigned char *data,
                                 unsigned int size)
{
    XmpData *xmp;
    
    xmp = xmp_data_new ();
    
    xmp_data_load_data (xmp, data, size);
    return (xmp);
}

void xmp_data_load_data (XmpData *data, const unsigned char *d,
                         unsigned int size)
{
    if (size < 33 || d[0] != 0xff || d[1] != 0xe1)
        return;
    
    if (memcmp(d + 4, XMP_IDENTIFIER_STR, strlen(XMP_IDENTIFIER_STR)) != 0)
        return;

    int offset = 4 + strlen(XMP_IDENTIFIER_STR) + 1;
    
    xmlInitParser();
    
    xmlDocPtr xml = xmlParseMemory((const char *)d + offset, size - offset);
    if (xml) {
        data->xml = xml;
    }

    xmlCleanupParser();
}

void xmp_data_save_data (XmpData *data, unsigned char **d,
                         unsigned int *ds)
{
   	if (ds)
        *ds = 0;
    
    if (!data || !d || !ds)
        return;

    long xml_version_prefix_length = 0;
    xmlChar *str = NULL;
    int padding = 2048;
    int size = 0;
    char *pos;
    xmlDocDumpMemoryEnc(data->xml, &str, &size, "UTF-8");
    if (str == NULL || strlen((char *)str) < xml_version_prefix_length)
        return;

    // Drop the "<?xml version="1.0"?>" prefix libxml2 adds
    if (strncmp((char *)str, "<?xml version", 5) == 0) {
        pos = strstr((char *)str, "?>");
        if (pos) {
            xml_version_prefix_length = (pos + 2) - (char *)str;
            if (strlen(pos) > 2 && pos[2] == '\n')
                xml_version_prefix_length++;
        }
    }
    str += xml_version_prefix_length;
    size -= xml_version_prefix_length;
    // Drop the trailing newline
    if (size > 1 && str[size - 1] == '\n')
        size--;
    
    int xml_offset = strlen(XMP_IDENTIFIER_STR) + 1;
    long end_packet_pos = size;
    pos = strstr((char *)str, "<?xpacket end");
    if (pos)
        end_packet_pos = (pos - 1) - (char *)str;

    *ds = xml_offset + size + padding;
    *d = xmp_data_alloc (data, *ds);
    if (!*d)  {
        *ds = 0;
        xmlFree (str);
        return;
    }

    int cursor = 0;
    memcpy (*d, XMP_IDENTIFIER_STR, xml_offset);
    cursor += xml_offset;

    memcpy (*d + cursor, str, end_packet_pos);
    cursor += end_packet_pos;

    memset(*d + cursor, 0x20, padding);
    cursor += padding;

    if (pos)
        memcpy (*d + cursor, &str[end_packet_pos + 1], size - end_packet_pos - 1);

    str -= xml_version_prefix_length;
    xmlFree (str);
}

void
xmp_data_ref (XmpData *data)
{
    if (!data)
        return;
    
    data->priv->ref_count++;
}

void
xmp_data_unref (XmpData *data)
{
    if (!data)
        return;
    
    data->priv->ref_count--;
    if (!data->priv->ref_count)
        xmp_data_free (data);
}

void xmp_data_free(XmpData *data)
{
    XmpMem *mem = (data && data->priv) ? data->priv->mem : NULL;
    
    if (!data)
        return;
    
    if (data->xml) {
        xmlFreeDoc(data->xml);
        data->xml = NULL;
    }
    
    if (data->priv) {
        xmp_mem_free (mem, data->priv);
        xmp_mem_free (mem, data);
    }
    
    xmp_mem_unref (mem);
}

void
xmp_data_dump (XmpData *data)
{
    if (!data)
        return;
    
    if (data->xml) {
        xmlDocDump(stdout, data->xml);
    }
}
