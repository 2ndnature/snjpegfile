# SNJPEGFile #

### Lightweight Objective-C library for updating JPEG metadata ###

Updating metadata using Apple's ImageIO functions is fairly easy - but doing so will recompress the image data. Which is a big no-no. See [this Tweet](https://twitter.com/2ndNatureDev/status/869483649024094208) as to why. (Besides it just being wrong to unnecessarily fiddle with the original bytes.)

After searching high and low, I wasn't able to find a library that was App Store friendly (non-GPL). So I thought I'd help my fellow indie developers out and put one together using modified [libexif](http://libexif.sourceforge.net), [libiptcdata](http://libiptcdata.sourceforge.net), and [exifyay](https://github.com/NarrativeTeam/exifyay) code (LGPL).
My own code is in the Public Domain. Do whatever you want with it. Earn a buck. Feed your family. Make the world a better place.

The mergeMetadata: method takes the same kind of dictionary of metadata keys and values you're used to from ImageIO.
Importing the SNJPEGFile+ConvenienceMethods.h file gives you a few extra convenience methods to losslessly rotate an image, wipe all metadata, wipe just the GPS data, add a GPS location from a CLLocation object, or resize a file keeping the original metadata.

Since no recompression is being done, changing metadata is about 10 times faster than using ImageIO.

Note: This is an extremely basic library. If you need a perfect fully fledged metadata library, consider paying the license fee for [Exiv2](http://www.exiv2.org).

If you're a developer, pull requests with improvements would be much appreciated.

### Adding it to your project ###

* Drag **SNJPEGFile.xcodeproj**, **SNJPEGFile+ConvenienceMethods.h**, and 
  **SNJPEGFile+ConvenienceMethods.m** into your Xcode project tree.
* Select your app under Targets and click *Build Phases*. Expand *Target Dependencies*
  and click the plus button to add the **SNJPEGFile** library.
* Expand *Link Binary With Libraries* and click the plus button to add **SNJPEGFile.a**. Click the plus button again and and **libxml2.2.tbd**

### Example code ###

```javascript
#import "SNJPEGFile+ConvenienceMethods.h"

SNJPEGFile *file = [[SNJPEGFile alloc] initWithFileURL:[NSURL fileURLWithPath:@"/IMG_0001.JPG"]];
[file setStarRating:4];
[file mergeMetadata:@{ (id)kCGImagePropertyTIFFDictionary:@{ (id)kCGImagePropertyTIFFImageDescription:@"Description" } }];
[file wipeGPS];
[file saveToFile:[NSURL fileURLWithPath:@"/output.jpg"]];
```