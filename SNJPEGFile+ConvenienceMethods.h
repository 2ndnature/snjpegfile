//
//  SNJPEGFile+ConvenienceMethods.h
//
//  Created by Brian Gerfort of 2ndNature on 14/05/2017.
//
//  This code is in the public domain.
//

#import <SNJPEGFile/SNJPEGFile.h>
#import <CoreLocation/CoreLocation.h>

@interface SNJPEGFile (ConvenienceMethods)

- (void)setStarRating:(NSUInteger)rating;
- (void)setGPSLocation:(nullable CLLocation *)location;
- (void)setCopyright:(nullable NSString *)copyright;
- (void)setCaption:(nullable NSString *)caption;
- (void)setByline:(nullable NSString *)byline;
- (void)setHeadline:(nullable NSString *)headline;
- (void)setUsageTerms:(nullable NSString *)usageTerms;
- (void)setKeywords:(nullable NSArray<NSString *> *)keywords;

- (void)wipeMetadata;
- (void)wipeGPS;

- (void)rotateClockwise;
- (void)rotateCounterclockwise;

+ (nullable NSURL *)fileByResizingFile:(nonnull NSURL *)fileURL toMaximumDimension:(NSUInteger)maxDimension;

@end
